import RandomNumber from './RandomNumber'

function App() {

  return (
    <>
      <h1>Random Number Generator</h1>
      <div>
        <RandomNumber />
      </div>

    </>
  )
}

export default App
