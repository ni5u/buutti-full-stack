const RandomNumber = () => {

    const randomNumber = Math.floor(Math.random() * 100 ) + 1

    return (
        <div className='RandomNumber'>
            Your Random Number is: {randomNumber}
        </div>
    )
}

export default RandomNumber