import { useState } from 'react'

function App() {
  const [count1, setCount1] = useState(0)
  const [count2, setCount2] = useState(0)
  const [count3, setCount3] = useState(0)

  return (
    <>
      <h1>Assignment 15.5</h1>
      <div className="counters">
        <p><button onClick={() => setCount1(count1 + 1)}>
          {count1}
        </button></p>
        <p><button onClick={() => setCount2(count2 + 1)}>
          {count2}
        </button></p>
        <p><button onClick={() => setCount3(count3 + 1)}>
          {count3}
        </button></p>
        <p>
          {count1 + count2 + count3}
        </p>
      </div>
    </>
  )
}

export default App
