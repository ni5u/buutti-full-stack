import { useState,useEffect } from 'react'
import './App.css'

function App() {

  const [user, setUser] = useState(0)
  const [grid, setGrid] = useState<string[]>(Array(9).fill(''))
  const [gameState, setGameState] = useState(0)
  const [round, setRound] = useState(1)
  const users = ['X', 'O']
  const gameStates = ['Player X turn', 'Player O turn', 'Player X won!', 'Player O won!', 'Tie! Game Over']

  const handleClick = (index: number) => {
    if(gameState >= 2) return

    if (grid[index] === '') {
      const newGrid = [...grid]
      newGrid[index] = users[user]
      setGrid(newGrid)
      setRound(round + 1)
      setUser((user + 1)%2)
    }    
  }

  const reset = () => {
    setGrid(Array(9).fill(''))
    setGameState(0)
    setRound(1)
    setUser(0)
  }

  useEffect(() => {
    setGameState(checkWin())
  }, [grid])

  const checkWin = () =>  {

    const winLines = [
      [0, 1, 2], // Top row
      [3, 4, 5], // Middle row
      [6, 7, 8], // Bottom row
      [0, 3, 6], // Left column
      [1, 4, 7], // Middle column
      [2, 5, 8], // Right column
      [0, 4, 8], // Left-to-right diagonal
      [2, 4, 6]  // Right-to-left diagonal
    ]

    for(const line of winLines) {
      const [a, b, c] = line

      if(grid[a] !== '' && grid[a] === grid[b] && grid[b] === grid[c]) {
        return users.indexOf(grid[a]) + 2 //['Player X turn', 'Player O turn', 'Player X won!', 'Player O won!', 'Tie! Game Over']
      }
    }

    if(round === 10) {
      return 4
    }

    return user
  }

  return (
    <>
      <h1>Tic Tac Toe</h1>
      <div className="turn">Round: {round} </div> 
      <div className="gameState">{gameStates[gameState]} { gameState >= 2  && <button onClick={reset}>Reset</button> } </div>
        <div className="game">
          { grid.map((cell, index) => (
            <div key={index} className="cell" onClick={() => handleClick(index)}>
              {cell}
            </div>
          )) }
      </div>
    </>
  )
}

export default App
