import './App.css'
import Names from './Names'
import namelist  from '../namelist'

function App() {

  return (
    <>
      <h1>Assignment 15.6</h1>
      <ul>
        <Names names={namelist}/>
      </ul>

    </>
  )
}

export default App
