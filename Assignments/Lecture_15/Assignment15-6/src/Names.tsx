
interface Props {
    names: string[]
}

function Names(props: Props) {

    return (
      <>
        {props.names.map((element, i) => {
            const content = i % 2 === 0 
            ?<b>{ element }</b>
            :<i>{ element }</i>

            return <li key={'names' + i}>{ content }</li>
                           
        } )  }
      </>
    )
  }
  
  export default Names