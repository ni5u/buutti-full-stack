import { useState, ChangeEvent } from 'react'

function UserInput() {
  const [myText, setMyText] = useState('')
  const [text, setText] = useState('')

  const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setMyText(event.target.value)
  }

  return (
    <div className='userInput'>
        <div className='outputInput'>{text}</div>
      <input name='textInput' type='text'
        value={myText}
        onChange={onInputChange} />
        <button name='button' onClick={() => setText(myText)}>Click</button>
      </div>
  )
}

export default UserInput