import { useState } from 'react'
import './App.css'
import UserInput from './UserInput'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      
      <h1>UserInput</h1>
      <div className="card">
        <UserInput />
      
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  )
}

export default App
