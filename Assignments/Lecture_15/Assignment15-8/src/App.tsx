import { useState, ChangeEvent } from 'react'
import './App.css'

interface Todo {
  id: number
  task: string
  done: boolean
  delete: boolean
}

function App() {

  const [newTodoId, setNewTodoId] = useState(0)
  const [todoText, setTodoText] = useState('')
  const [todos, setTodos] = useState<Todo[]>([])

  const onInputChange =  (event: ChangeEvent<HTMLInputElement>) => {
    setTodoText(event.target.value)
  }

  const addNewTodo = () => {
    const todo: Todo = {
      id: newTodoId,
      task: todoText,
      done: false,
      delete: false
    }

    setTodos([...todos, todo])
    setTodoText('')  
    setNewTodoId(newTodoId + 1)
  }

  const updateTodo = (id: number, remove: boolean ) => {
    const updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        if (remove) {
          return { ...todo, delete: true } // mark todo to be deleted
        }
        return { ...todo, done: !todo.done }  // mark todo as done
      }
      return todo; // Keep other elements unchanged
    });

    // remove todos marked to be deleted
    const newTodos = remove ? updatedTodos.filter((todo) => !todo.delete) : updatedTodos;

    setTodos(newTodos)

  };

  return (
    <>
      <h1>Todos</h1>
      <h2>Todo</h2>
      <ul className="todoList">
        {
          todos.filter((todo) => !todo.done).map((todo) => {
            return <li key={ 'todo' + todo.id }> 
                      
                      <input type="checkbox" checked={ todo.done } onChange={() => updateTodo(todo.id, false)}></input> 
                      { todo.task } 
                      <button onClick={() => updateTodo(todo.id, true)}>X</button> 
                    </li>
          })
        }        
      </ul>
      <h2>Done</h2>
      <ul className="doneList">
        {
          todos.filter((todo) => todo.done).map((todo) => {
            return <li key={ 'todo' + todo.id }>
                      <s> 
                        <input type="checkbox" checked={ todo.done } onChange={() => updateTodo(todo.id, false)}></input> 
                        { todo.task }
                      </s>
                      <button onClick={() => updateTodo(todo.id, true)}>X</button>
                    </li>
          })
        }        
      </ul>
      <div>
        <input name='newTodoText' type='text'
        placeholder='Write a new todo'
        value={ todoText }
        onChange={ onInputChange } />
        <button name='addNewTodoButton' onClick={() => addNewTodo()} >Add</button>
      </div>
    </>
  )
}

export default App
