import guitarImg from '../img/guitar.png'
import tubaImg from '../img/tuba.png'
import violinImg from '../img/violin.png'
import Instrument from './Instrument'


function App() {

  return (
    <>      
      <h1>Instruments</h1>
      <div className="instruments">
        <Instrument name="guitar" image={guitarImg} price={100} />
        <Instrument name="violin" image={violinImg} price={150} />
        <Instrument name="tuba" image={tubaImg} price={300} />
      </div>
     
    </>
  )
}

export default App
