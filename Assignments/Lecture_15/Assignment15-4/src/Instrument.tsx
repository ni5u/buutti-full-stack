import './Instrument.css'

interface Props {
    name: string
    image: string
    price: number
}


function Instrument(props: Props) {

    return (
      <>        
        <h2 className="h2instrument">{props.name}</h2>
        <p className="price"> Price: {props.price}</p>
        <img className="image" src={props.image}></img>
      </>
    )
  }
  
  export default Instrument