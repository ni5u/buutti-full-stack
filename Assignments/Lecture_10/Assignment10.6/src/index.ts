import express, {Request, Response} from 'express'

const server = express() 


server.get('/', (req: Request, res: Response) => {
    res.status(200).send('Hello from Docker!')
})



server.listen(3000, () => {
    console.log('Listening to port 3000')
})