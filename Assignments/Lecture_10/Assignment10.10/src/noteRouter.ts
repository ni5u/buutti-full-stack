import express, { Request, Response} from 'express'
import { validatePost} from './middleware'

const noteRouter = express.Router() 
const notes: {'id': number, content: string }[] = []

noteRouter.get('/', (_req: Request, res: Response) => {
    res.send(notes)   
})

noteRouter.get('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const note = notes.find((item) => item.id === id)
    if(!note) {
        res.status(404).send('Id not found')
    } else {  
        res.status(200).send(note.content)   
    }
})
noteRouter.post('/', validatePost)
noteRouter.post('/', (req: Request, res: Response) => {
    const id = Number(req.body.id)
    const content = req.body.content
    if(notes.find((item) => item.id === id)) {
        res.status(400).send('Duplicate id!')
    } else {
        notes.push({'id': id, 'content': content})
        res.status(201).send()
    }        
})

noteRouter.delete('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = notes.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    } else {
        notes.splice(notes.indexOf(student))
        res.status(204).send(student)
    }      
})

export default noteRouter