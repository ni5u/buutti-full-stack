import express from 'express'
import {unknownEndpoint} from './middleware'
import noteRouter from './noteRouter'
import { authenticate } from './authenticate'

const server = express()
server.use(express.json())
server.use('/note', noteRouter)
server.use('/secret', authenticate)
server.use('/secret', noteRouter)
server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})