import { Request, Response, NextFunction } from 'express'

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Page not found!'})
}

export const validatePost =  (req: Request, res: Response, next: NextFunction) => {
    if(!('id' in req.body && 'content' in req.body)) {
        res.status(400).send('Missing parameters: Use: id, content')
    } else {
        next()
    }
}