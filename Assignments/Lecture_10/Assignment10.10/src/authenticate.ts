import { NextFunction, Request, Response} from 'express'
import argon from 'argon2'

const users: {'username': string, 'hash': string }[] = []
const username = process.env.SECRETUSERNAME??''
const userPwd = process.env.USERPWD??''

users.push({'username': username, 'hash': userPwd})

export const authenticate = (req: Request, res: Response, next: NextFunction) => {
    const {username, password} = req.body

    if((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    const user = users.find((user) => user.username === username)
    if(user) {
        argon.verify(user.hash,password)
            .then((result) => {
                if(result) {
                    next()
                } else {
                    res.status(401).send()
                }                 
            }) 
    } else {
        res.status(401).send('No such user')
    }
    
}