"use strict";
const fibonacci = (n = 0) => {
    const cache = [0, 1];
    if (n === 0 || n === 1) {
        return cache[n];
    }
    if (cache[n] !== undefined) {
        return cache[n];
    }
    else {
        const result = fibonacci(n - 1) + fibonacci(n - 2);
        cache.push(result);
        return result;
    }
};
let fibMax = Number(process.argv[2]);
let n = 0;
setInterval(() => console.log(fibonacci(n++)), 1000);
