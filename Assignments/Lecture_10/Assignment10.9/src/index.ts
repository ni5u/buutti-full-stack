import express from 'express'
import {unknownEndpoint} from './middleware'
import noteRouter from './noteRouter'

const server = express()

server.use(express.json())
server.use('/note', noteRouter)
server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})