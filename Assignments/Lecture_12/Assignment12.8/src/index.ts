import express from 'express'
import { createProductsTable } from './db'
import router from './productrouter'
const { PORT } = process.env

const server = express()

createProductsTable()
server.use(express.json())
server.use('/product', router)

server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})