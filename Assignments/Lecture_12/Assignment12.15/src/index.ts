import express from 'express'
import router from './forumRouter'
import { unknownEndpoint } from './middleware'

const { PORT } = process.env

const server = express()

server.use(express.json())
server.use('/forum', router)
server.use('/', unknownEndpoint)

server.listen(PORT, () => {
    console.log('forum API listening to port', PORT)
})