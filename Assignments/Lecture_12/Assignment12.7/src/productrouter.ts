import { Router, Request, Response } from 'express'
import { deleteProduct, getOneProduct, getProducts, insertProduct, updateProduct } from './dao'

interface Product {
    name: string
    price: number
}

const router = Router()

router.post('/', async (req: Request, res: Response) => {
    const product: Product = req.body
    const result = await insertProduct(product.name, product.price)
    res.status(201).send(result)
})

router.put('/:id', async (req: Request, res: Response) => {
    const id: number = Number(req.params.id)
    const product: Product = {
        name: req.body.name,
        price: req.body.price
    }
    const result = await updateProduct(id, product)
    res.status(200).send(result.rows)
})

router.get('/', async (req: Request, res: Response) => {
    const result = await getProducts()
    res.status(200).send(result)
})

router.get('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await getOneProduct(id)
    res.status(200).send(result)
})

router.delete('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await deleteProduct(id)
    res.status(200).send(result.rows)
})


export default router