UPDATE users
set email = 'nisu@nisula.com'
where user_id = 1;


-- foreign keyden takia pitää poistella kommentteja ja postauksia ennen kuin käyttäjän voi poistaa, jotta tietokannan integriteetti säilyisi
delete from comments 
where post_id = 4

delete from posts
where user_id = 2

delete from users
where user_id = 2