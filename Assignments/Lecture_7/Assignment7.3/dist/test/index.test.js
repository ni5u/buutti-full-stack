"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../src/index");
test('dummy test', () => {
    expect(true).toBe(true);
});
test('multiply 0*0', () => {
    expect((0, index_1.calculator)('*', 0, 0)).toBe(0);
});
test('multiply 1*1', () => {
    expect((0, index_1.calculator)('*', 1, 1)).toBe(1);
});
test('multiply 0.1*0.1', () => {
    expect((0, index_1.calculator)('*', 0.1, 0.1)).toBe(0.01);
});
test('multiply 5*5', () => {
    expect((0, index_1.calculator)('*', 5, 5)).toBe(25);
});
