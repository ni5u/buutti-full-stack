
export function calculator(operator: string, num1: number, num2: number): number | string {
    switch (operator) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;
        default:
            return 'Invalid operator';
    }

}

console.log(calculator('*',0.1,0.1));