import {calculator} from '../src/index';

test('dummy test', () => {
    expect(true).toBe(true);
});

test('multiply 0*0', () => {
    expect(calculator('*',0,0)).toBe(0);
});

test('multiply 1*1', () => {
    expect(calculator('*',1,1)).toBe(1);
});

test('multiply 0.1*0.1', () => {
    expect(calculator('*',0.1,0.1)).toBe(0.01);
});

test('multiply 5*5', () => {
    expect(calculator('*',5,5)).toBe(25);
});