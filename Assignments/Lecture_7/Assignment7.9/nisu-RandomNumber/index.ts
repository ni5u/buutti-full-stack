import { AzureFunction, Context, HttpRequest } from "@azure/functions"

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request.');
    const min: number = req.body.min;
    const max: number = req.body.max;
    const isInteger: boolean = req.body.isInteger;
    let responseMessage = 'Your random number: ';
    let status = 200;

    if(!('min' in req.body && 'max' in req.body && 'isInteger' in req.body)) {
        responseMessage = 'Missing parameter! Use: min: number, max: number: isInteger: boolean';
        responseMessage += `\nmin: ${min} max: ${max} isInteger ${isInteger}`;
        status = 400;
    } else {
        const answer = Math.random() * (max - min) + min;
        if(!isInteger) {
            responseMessage += answer;
        } else {
            responseMessage += Math.floor(answer);
        }
    }

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage, status
    };

};

export default httpTrigger;