import {calculator} from '../src/index';

describe('Calculator', () => {

    it('dummy works', () => {
        const result = true;
        expect(result).toBe(true);
    });

    it('Returns 4 with parameters "+", 2 and 2', () => {
        const result = calculator('+',2,2);
        expect(result).toBe(4);
    });
   
    it('Returns 1 with parameters "-", 2 and 1', () => {
        const result = calculator('-', 2, 1);
        expect(result).toBe(1);
    });

    it('Returns 0 with parameters "*", 0 and 1000', () => {
        const result = calculator('*', 0, 1000);
        expect(result).toBe(0);
    });

    it('Returns "Invalid operator" with parameters "g", 1 and 1', () => {
        const result = calculator('g', 1, 1);
        expect(result).toBe('Invalid operator');
    });

    it('Returns 0.1 with parameters "/", 1 and 10', () => {
        const result = calculator('/', 1, 10);
        expect(result).toBe(0.1);
    });

});
 