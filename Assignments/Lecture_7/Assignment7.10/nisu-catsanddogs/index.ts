import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import axios from 'axios'
let requestNumber = 0;

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request.');
    const catUrlBase = 'https://cataas.com/';
    const catUrlJSON = 'cat?json=true';
    const dogUrl = 'https://dog.ceo/api/breeds/image/random';
    const foxUrl = 'https://randomfox.ca/floof/';
    let responseMessage = '';
    let message = 'Error?';
    let link = 'https://no.link.found';

    requestNumber++;
    const isFox = requestNumber % 13 === 0
    if(isFox) {
        const response = (await axios.get(foxUrl)).data;
        link = response.image;
        message = 'Surprise fox!';
        responseMessage = JSON.stringify({message, link});
                          
    } else {
        const isCat = Math.random() < 0.5;
        if(isCat) {
            const response = (await axios.get(catUrlBase + catUrlJSON)).data; 
            const tags = response.tags;
            if(tags.length === 0) {
                message = 'Random cat';
            } else {
                message = 'Cat ' + tags[Math.floor(Math.random() * tags.length)];
            }
            link = catUrlBase + response.url;            
        } else {
            const response = (await axios.get(dogUrl)).data;
            message = "Here's a random dog for you!";
            link = response.message;
        }
    }
    responseMessage = JSON.stringify({message, link});
    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage
    };    

};

export default httpTrigger;

// Ei näistä kommentoitavaa, keep up the good work!