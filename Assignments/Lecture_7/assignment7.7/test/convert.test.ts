import {convert} from '../src/convert';

describe('Convert', () => {

    it('dummy works', () => {
        const result = true;
        expect(result).toBe(true);
    });

    it('Returns 0.1 with parameters 1, "dl" and "l"' , () => {
        const result = convert(1, 'dl', 'l');
        expect(result).toBe(0.1);
    });

    it('Returns 0.1 with parameters 1, "dl" and "l"' , () => {
        const result = convert(1, 'dl', 'l');
        expect(result).toBe(0.1);
    });

    it('Returns 20.228 with parameters 6, "dl" and "oz"' , () => {
        const result = convert(6, 'dl', 'oz');
        expect(result).toBeCloseTo(20.288, 3);
    });

    it('Returns 0.266 with parameters 9, "oz" and "l"' , () => {
        const result = convert(9, 'oz', 'l');
        expect(result).toBeCloseTo(0.266, 3);
    });

    it('Returns 4731.7647 with parameters 1000, "pt" and "dl"' , () => {
        const result = convert(1000, 'pt', 'dl');
        expect(result).toBeCloseTo(4731.7647, 3);
    });

});
 