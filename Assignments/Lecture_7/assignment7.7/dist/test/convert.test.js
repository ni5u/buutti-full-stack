"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const convert_1 = require("../src/convert");
describe('Convert', () => {
    it('dummy works', () => {
        const result = true;
        expect(result).toBe(true);
    });
    it('Returns 0.1 with parameters 1, "dl" and "l"', () => {
        const result = (0, convert_1.convert)(1, 'dl', 'l');
        expect(result).toBe(0.1);
    });
});
