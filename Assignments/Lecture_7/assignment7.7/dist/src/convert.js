"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.convert = void 0;
function convert(amount, sourceUnit, targetUnit) {
    const units = {
        dl: 1,
        l: 10,
        oz: 0.295735296,
        cup: 2.36588237,
        pt: 4.73176473
    };
    return units[sourceUnit] / units[targetUnit] * amount;
}
exports.convert = convert;
console.log(convert(Number(process.argv[2]), process.argv[3], process.argv[4]));
