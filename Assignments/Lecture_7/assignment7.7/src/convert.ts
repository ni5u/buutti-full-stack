
export function convert(amount: number, sourceUnit: string, targetUnit: string): number {
    const units: { [key: string]: number }  = {
        dl: 1,
        l: 10,
        oz: 0.295735296,
        cup: 2.36588237,
        pt: 4.73176473
    };

    return units[sourceUnit]/units[targetUnit] * amount;
}

console.log(convert(Number(process.argv[2]), process.argv[3], process.argv[4]));