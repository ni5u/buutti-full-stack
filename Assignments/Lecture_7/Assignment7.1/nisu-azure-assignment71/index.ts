import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import { test } from "node:test";

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request.');
    const input:string = req.body.input;
    let status: number;
    let body: string;
    if(!input) {
        status = 400;
        body = 'Missing parameter: "input"'
    } else {
        status = 200;
        body = input.toUpperCase();
    }
    context.res = {status, body};

};

export default httpTrigger;