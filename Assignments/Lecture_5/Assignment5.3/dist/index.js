function Ingredient(name, amount) {
    this.name = name;
    this.amount = amount;
    /*this.toString = function() {
        return this.name + ' ' + this.amount;
    };*/
}
Ingredient.prototype.scale = function (scale) {
    this.amount *= scale;
};
const flour = new Ingredient('flour', 1);
const milk = new Ingredient('milk', 1);
console.log(flour, milk);
flour.scale(2);
milk.scale(3);
console.log(flour, milk);
function Recipe(name, ingredients, servings) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
}
Recipe.prototype.toString = function () {
    return `Recipe: ${this.name}, servings: ${this.servings}, ingredients: ${this.ingredients.map(ele => ele.name + ' ' + ele.amount)}`;
};
Recipe.prototype.setServings = function (newServings) {
    for (let i = 0; i < this.ingredients.length; i++) {
        this.ingredients[i].scale(newServings / this.servings);
    }
    this.servings = newServings;
};
const bread = new Recipe('bread', [flour, milk], 3);
console.log(bread.toString());
bread.setServings(5);
console.log(bread.toString());
