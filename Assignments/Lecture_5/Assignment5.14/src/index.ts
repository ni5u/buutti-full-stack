function generateCredentials(firstName: string, lastName: string): string[] {
    
    const year =  new Date().getFullYear().toString().slice(2);    
    const letterIndex = Math.floor(Math.random() * (122-97)) + 97;
    const specialIndex = Math.floor(Math.random() * (47-33)) + 33;

    const username = 'B' + year + lastName.slice(0,2).toLowerCase() + firstName.slice(0,2).toLowerCase();
    const password = String.fromCharCode(letterIndex)
                    + firstName[0].toLowerCase()
                    + lastName[lastName.length-1].toUpperCase()
                    + String.fromCharCode(specialIndex) 
                    + year;

    return [username, password];
}

console.log(generateCredentials('Sami', 'Nissinen'));