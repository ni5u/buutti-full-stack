function collatz(n: number, step = 0): number[] | number {
    if(n === 1) {
        return step;
    } else if(n % 2 === 0) {
        return collatz(n / 2, step+1);
    } else {
        return collatz(n*3+1, step+1);
    }
}

console.log(collatz(Number(process.argv[2])));

// Eipä näissä paljon kommentoitavaa ole. Erinomaista koodia.