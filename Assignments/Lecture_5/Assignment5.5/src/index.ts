class Ingredient {
    name: string;
    amount: number;

    constructor(name: string, amount: number) {
        this.name = name;
        this.amount = amount;
    }

    scale (scale: number) {
        this.amount *= scale;
    }
}

const flour = new Ingredient('flour', 1);
const milk = new Ingredient('milk', 1 );
const chili = new Ingredient('chili', 1);

console.log(flour, milk);
flour.scale(2);
milk.scale(3);
console.log(flour, milk);

class Recipe {
    name: string;
    ingredients: Ingredient[];
    servings: number;

    constructor(name: string, ingredients: Ingredient[], servings: number) {
        this.name = name;
        this.ingredients = ingredients;
        this.servings = servings;
    }
    toString(){
        return `Recipe: ${this.name}, servings: ${this.servings}, ingredients: ${this.ingredients.map(ele => ele.name + ' ' + ele.amount.toFixed(2))}`;
    }
    
    setServings (newServings: number) {
        for(let i = 0; i < this.ingredients.length; i++) {
            this.ingredients[i].scale(newServings/this.servings);
        }
        this.servings = newServings;
    }

}

class HotRecipe extends Recipe {
    heatLevel: number;

    constructor(name: string, ingredients: Ingredient[], servings: number, heatLevel: number) {
        super(name, ingredients, servings);
        this.heatLevel = heatLevel;
    }

    toString(){
        let outString = super.toString(); 
        if(this.heatLevel > 5) {
            outString += '\nWarning! Hot recipe! Heatlevel: ' + this.heatLevel;
        }
        return outString;
    }
    
}

const mildBread = new HotRecipe('bread', [flour, milk], 3,1);
const hotBread = new HotRecipe('hotBread', [flour, milk, chili], 3, 6);

console.log(mildBread.toString());
console.log(hotBread.toString());
mildBread.setServings(100);
hotBread.setServings(100);

console.log(mildBread.toString());
console.log(hotBread.toString());


