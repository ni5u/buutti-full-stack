"use strict";
class Ingredient {
    constructor(name, amount) {
        this.name = name;
        this.amount = amount;
    }
    scale(scale) {
        this.amount *= scale;
    }
}
const flour = new Ingredient('flour', 1);
const milk = new Ingredient('milk', 1);
console.log(flour, milk);
flour.scale(2);
milk.scale(3);
console.log(flour, milk);
class Recipe {
    constructor(name, ingredients, servings) {
        this.name = name;
        this.ingredients = ingredients;
        this.servings = servings;
    }
    toString() {
        return `Recipe: ${this.name}, servings: ${this.servings}, ingredients: ${this.ingredients.map(ele => ele.name + ' ' + ele.amount)}`;
    }
    setServings(newServings) {
        for (let i = 0; i < this.ingredients.length; i++) {
            this.ingredients[i].scale(newServings / this.servings);
        }
        this.servings = newServings;
    }
}
const bread = new Recipe('bread', [flour, milk], 3);
console.log(bread.toString());
bread.setServings(5);
console.log(bread.toString());
