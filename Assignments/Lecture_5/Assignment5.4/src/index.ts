class Ingredient {
    name: string;
    amount: number;

    constructor(name: string, amount: number) {
        this.name = name;
        this.amount = amount;
    }

    scale (scale: number) {
        this.amount *= scale;
    }
}

const flour = new Ingredient('flour', 1);
const milk = new Ingredient('milk', 1 );

console.log(flour, milk);
flour.scale(2);
milk.scale(3);
console.log(flour, milk);

class Recipe {
    name: string;
    ingredients: Ingredient[];
    servings: number;

    constructor(name: string, ingredients: Ingredient[], servings: number) {
        this.name = name;
        this.ingredients = ingredients;
        this.servings = servings;
    }
    toString(){
        return `Recipe: ${this.name}, servings: ${this.servings}, ingredients: ${this.ingredients.map(ele => ele.name + ' ' + ele.amount)}`;
    }
    
    setServings (newServings: number) {
        for(let i = 0; i < this.ingredients.length; i++) {
            this.ingredients[i].scale(newServings/this.servings);
        }
        this.servings = newServings;
    }

}

const bread = new Recipe('bread', [flour, milk], 3);

console.log(bread.toString());
bread.setServings(5);
console.log(bread.toString());