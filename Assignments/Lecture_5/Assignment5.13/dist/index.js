"use strict";
function checkExam(correct, student) {
    let points = 0;
    for (let i = 0; i < correct.length; i++) {
        if (student[i] === '') {
            points += 0; // needed to exclude condition from elses. could be empty block, but points += 0 to show the logic
        }
        else if (correct[i] === student[i]) {
            points += 4;
        }
        else {
            points--;
        }
    }
    return points > 0 ? points : 0;
}
const ref = ['a', 'b', 'c', 'd'];
const student = ['a', 'b', 'q', 'q'];
console.log(checkExam(ref, student));
