"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const song = fs_1.default.readFileSync('./laulu.txt', 'utf-8');
let updatedSong = '';
for (const word of song.split(' ')) {
    if (word === 'joulu') {
        updatedSong += 'kinkku';
    }
    else if (word === 'lapsilla') {
        updatedSong += 'poroilla';
    }
    else if (word === 'Joulu') {
        updatedSong += 'Kinkku';
    }
    else if (word === 'Lapsilla') {
        updatedSong += 'Poroilla';
    }
    else {
        updatedSong += word;
    }
    updatedSong += ' ';
    console.log('-' + word) + '-';
}
//console.log(song);
console.log(updatedSong);
fs_1.default.writeFileSync('./uuslaulu.txt', updatedSong, 'utf-8');
