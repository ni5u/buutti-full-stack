import fs from 'fs';

const song = fs.readFileSync('./laulu.txt', 'utf-8');
let updatedSong = '';

for(const word of song.split(' ')) {
    if(word === 'joulu') {
        updatedSong += 'kinkku';
    } else if(word === 'lapsilla') {
        updatedSong += 'poroilla';
    } else if(word === 'Joulu') {
        updatedSong += 'Kinkku';
    } else if(word === 'Lapsilla') {
        updatedSong += 'Poroilla';
    } else {
        updatedSong += word;
    }
    updatedSong += ' ';
    console.log('-' + word) + '-';
}
//console.log(song);
console.log(updatedSong);
fs.writeFileSync('./uuslaulu.txt', updatedSong, 'utf-8');