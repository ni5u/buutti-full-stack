function Ingredient(name: string, amount: number) {
    this.name = name;
    this.amount = amount;
    /*this.toString = function() {
        return this.name + ' ' + this.amount;
    };*/
}

const flour = new Ingredient('flour', 1);
const milk = new Ingredient('milk', 1 );
//console.log(flour, milk);

function Recipe(name: string, ingredients:  Array<{name: string, amount: number }>[], servings: number) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
    this.toString = function() {
        return `Recipe: ${this.name}, servings: ${this.servings}, ingredients: ${this.ingredients.map(ele => ele.name + ' ' + ele.amount)}`;
    };
}

const bread = new Recipe('bread', [flour, milk], 3);

console.log(bread.toString());