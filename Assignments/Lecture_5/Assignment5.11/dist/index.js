"use strict";
function aboveAverage(arr) {
    const average = arr.reduce((acc, cur) => acc + cur, 0) / arr.length;
    return arr.filter((ele => ele > average));
}
const nums = [1, 2, 3, 4, 5, 6, 7];
console.log(aboveAverage(nums));
