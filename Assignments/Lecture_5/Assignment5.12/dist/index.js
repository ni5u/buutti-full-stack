"use strict";
function getVowelCount(inputString) {
    return inputString.split('').reduce((acc, cur) => acc + ('aeiouy'.includes(cur) ? 1 : 0), 0);
}
console.log(getVowelCount('antti osti uuden auton'));
