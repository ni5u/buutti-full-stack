"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.unknownEndpoint = void 0;
const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: 'Nothing here!' });
};
exports.unknownEndpoint = unknownEndpoint;
