import express, { Request, Response } from 'express'

interface Event {
    id: number,
    title: string,
    description: string,
    date: Date,
    time?: string
}

const events: Event[] = []

const calendarRouter = express.Router()

calendarRouter.post('/', (req: Request, res: Response) => {
    const id = Number(req.body.id)
    if (id === undefined) {
        res.status(400).send('No id!')
    }

    if (events.find((event) => event.id === id)) {
        return res.status(400).send('Duplicate event id!')
    }
    // ID:t kannattaa lähtökohtasesti generoida bäkkärin puolella, ja olettaa että käyttäjä antaa vaan datan. Sillon voidaan itse kontrolloida id:n formatti ja pitää huolta että ei tuu duplikaatteja
    const dateString = req.body.date ?? '1.1.2000'
    const [day, month, year] = dateString.split('.')

    const isoDateString = `${year}-${month}-${day}`

    const date = new Date(Date.parse(isoDateString))

    const event: Event = {
        id: Number(req.body.id),
        title: req.body.title,
        description: req.body.description,
        date: date,
        time: req.body.time
    }
    events.push(event) // mielummin immutaabeli concat
    res.status(200).send()
})

calendarRouter.put('/:id', (req: Request, res: Response) => {

    const id = Number(req.params.id)
    const event = events.find((event) => event.id === id)
    if (event === undefined) {
        return res.status(404).send('Event not found!')
    } else { // if-haarassa on return, joten else on tarpeeton
        const title = req.body.title
        const description = req.body.author
        const date = req.body.date
        const time = req.body.time
        event['title'] = title ? title : event['title']
        event['description'] = description ? description : event['description']
        event['date'] = date ? date : event['date']
        event['time'] = time ? time : event['time']
        res.status(204).send()
        return // return viimeisenä rivinä tarpeeton
    }
})

calendarRouter.get('/', (req: Request, res: Response) => {
    console.log(req.body) // turha loggaus
    res.status(200).send(events)
})

calendarRouter.get('/:monthNumber', (req: Request, res: Response) => {
    const month = Number(req.params.monthNumber) - 1
    const eventsOnMonth = events.filter((event) => event.date.getMonth() === month)
    if (eventsOnMonth) { // .filter palauttaa aina listan, ja myös tyhjä lista on truthy, joten tää koodi menee aina tähän if-haaraan
        return res.status(200).send(eventsOnMonth)
    }
    res.status(404).send('No events found!')
})

calendarRouter.delete('/:id', (req: Request, res: Response) => {

    const id = Number(req.params.id)
    for (const event of events) {
        if (event.id === id) {
            events.splice(events.indexOf(event), 1)
            res.status(204).send()
            return
        }
    }
    // immutaabeli .filter()
    res.status(404).send('Event not found!')
})

export default calendarRouter