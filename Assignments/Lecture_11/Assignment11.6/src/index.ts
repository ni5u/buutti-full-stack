import express from 'express'
import { unknownEndpoint } from './middleware'
import calendarRouter from './calendarrouter'

const PORT = process.env.PORT ?? 3000

const server = express()

server.use(express.json())
server.use('/', calendarRouter)
server.use(unknownEndpoint)

server.listen(PORT, () => {
    console.log('Listening to port 3000') // kannattaa tohon loggaukseen kans laittaa PORT, muuten ihmettelet jossain vaiheessa et miks ihmeessä tää portti ei vaihdu
})