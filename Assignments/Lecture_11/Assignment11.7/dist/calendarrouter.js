"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const events = [];
const calendarRouter = express_1.default.Router();
calendarRouter.post('/', (req, res) => {
    var _a;
    const id = Number(req.body.id);
    if (id === undefined) {
        res.status(400).send('No id!');
    }
    if (events.find((event) => event.id === id)) {
        return res.status(400).send('Duplicate event id!');
    }
    const dateString = (_a = req.body.date) !== null && _a !== void 0 ? _a : '1.1.2000';
    const [day, month, year] = dateString.split('.');
    const isoDateString = `${year}-${month}-${day}`;
    const date = new Date(Date.parse(isoDateString));
    const event = {
        id: Number(req.body.id),
        title: req.body.title,
        description: req.body.description,
        date: date,
        time: req.body.time
    };
    events.push(event);
    res.status(200).send();
});
calendarRouter.put('/:id', (req, res) => {
    const id = Number(req.params.id);
    const event = events.find((event) => event.id === id);
    if (event === undefined) {
        return res.status(404).send('Book not found!');
    }
    else {
        const title = req.body.title;
        const description = req.body.author;
        const date = req.body.date;
        const time = req.body.time;
        event['title'] = title ? title : event['title'];
        event['description'] = description ? description : event['description'];
        event['date'] = date ? date : event['date'];
        event['time'] = time ? time : event['time'];
        res.status(204).send();
        return;
    }
});
calendarRouter.get('/', (req, res) => {
    console.log(req.body);
    res.status(200).send(events);
});
calendarRouter.get('/:monthNumber', (req, res) => {
    const month = Number(req.params.monthNumber) - 1;
    const eventsOnMonth = events.filter((event) => event.date.getMonth() === month);
    if (eventsOnMonth) {
        return res.status(200).send(eventsOnMonth);
    }
    res.status(404).send('No events found!');
});
calendarRouter.delete('/:id', (req, res) => {
    const id = Number(req.params.id);
    for (const event of events) {
        if (event.id === id) {
            events.splice(events.indexOf(event), 1);
            res.status(204).send();
            return;
        }
    }
    res.status(404).send('Event not found!');
});
exports.default = calendarRouter;
