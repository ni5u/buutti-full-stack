"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const middleware_1 = require("./middleware");
const calendarrouter_1 = __importDefault(require("./calendarrouter"));
const PORT = (_a = process.env.PORT) !== null && _a !== void 0 ? _a : 3000;
const server = (0, express_1.default)();
server.use(express_1.default.json());
server.use('/', calendarrouter_1.default);
server.use(middleware_1.unknownEndpoint);
server.listen(PORT, () => {
    console.log('Listening to port 3000');
});
