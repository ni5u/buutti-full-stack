"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const PORT = (_a = process.env.PORT) !== null && _a !== void 0 ? _a : 3000;
const server = (0, express_1.default)();
server.get('/', (_req, res) => {
    res.status(200).send('ok');
});
server.listen(PORT, () => {
    console.log('Listening to port 3000');
});
