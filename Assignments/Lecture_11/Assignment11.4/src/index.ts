
import express, {Request, Response} from 'express'
import send from 'send'

const PORT = process.env.PORT ?? 3000
const server = express()

server.get('/',(_req: Request, res: Response)=> {
    res.status(200).send('ok')
})


server.listen(PORT, () => {
    console.log('Listening to port 3000')
})