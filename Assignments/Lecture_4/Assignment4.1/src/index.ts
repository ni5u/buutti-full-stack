function capitalize(text: string) {
	const stringArray = text.split(" ")
	let capitalizedString = ""
	const capitalizedArray:string[] = []

	// original solution
	//	for(const word of stringArray) {
	//		capitalizedString += word[0].toUpperCase() + word.slice(1) + " "
	//	}
	// with forEach
	stringArray.forEach((word) => {
		capitalizedArray.push(word[0].toUpperCase() + word.slice(1))
	})
	capitalizedString = capitalizedArray.join(" ")
	console.log(capitalizedString)
}

capitalize("this is a string")