"use strict";
function prime(n) {
    const nSqrt = Math.sqrt(n);
    if (n < 2)
        return false;
    for (let i = 2; i <= nSqrt; i++) {
        if (n % i === 0)
            return false;
    }
    return true;
}
// Testing
const primes = [];
for (let j = 0; j < 100; j++) {
    if (prime(j)) {
        primes.push(j);
    }
}
console.log(primes);
