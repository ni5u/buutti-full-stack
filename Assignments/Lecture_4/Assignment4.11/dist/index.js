"use strict";
const names = [
    "Murphy",
    "Hayden",
    "Parker",
    "Arden",
    "George",
    "Andie",
    "Ray",
    "Storm",
    "Tyler",
    "Pat",
    "Keegan",
    "Carroll"
];
const doJoin = (strings, separator) => strings.reduce((acc, cur) => acc + separator + cur);
console.log(doJoin(names, " "));
