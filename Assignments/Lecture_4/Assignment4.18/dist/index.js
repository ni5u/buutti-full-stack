"use strict";
const word = process.argv[2];
function palindrome(word) {
    for (let i = 0; i < word.length / 2; i++) {
        if (word[i] !== word[word.length - 1 - i]) {
            return false;
        }
    }
    return true;
}
if (palindrome(word)) {
    console.log(`Yes, '${word}' is a palindrome`);
}
else {
    console.log(`No, '${word}' is not a palindrome`);
}
