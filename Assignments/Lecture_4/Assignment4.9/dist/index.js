"use strict";
const numbers = [
    749385,
    498654,
    234534,
    345467,
    956876,
    365457,
    235667,
    464534,
    346436,
    873453
];
const filtered = numbers.filter((ele) => {
    return !(ele % 5 === 0 && ele % 3 === 0) && ((ele % 3 === 0) || (ele % 5 === 0));
});
console.log(filtered);
