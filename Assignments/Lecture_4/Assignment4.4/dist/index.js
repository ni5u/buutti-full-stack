"use strict";
const diceGenerator = (sides) => {
    const dice = (rolls) => {
        let total = 0;
        for (let roll = 0; roll < rolls; roll++) {
            total += Math.floor(Math.random() * sides) + 1;
        }
        return total;
    };
    return dice;
};
const d6 = diceGenerator(6);
const d8 = diceGenerator(8);
for (let i = 0; i < 20; ++i) {
    console.log(`Damage: ${d6(2) + d8(2)}`);
}
