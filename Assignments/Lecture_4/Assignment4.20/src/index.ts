function fib(n: number): number {
	if(n === 0) return 0
	if(n < 2) return 1
	return fib(n-1) + fib(n-2)
}




function fibSequence(length:number):number[] {
	const fibArray = []
	for(let i = 0; i < length; i++) {
		fibArray.push(fib(i))
	}
	return fibArray
}

console.log(fibSequence(10))
