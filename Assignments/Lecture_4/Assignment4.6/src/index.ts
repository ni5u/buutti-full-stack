const factorial = (n: number): number => {
	if(n === 1) {
		return 1
	} else {
		return factorial(n-1) * n
	}
}

console.log(factorial(5))
