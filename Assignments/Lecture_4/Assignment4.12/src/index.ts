const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]

// part 1
const divByThree = arr.filter((ele) => ele%3==0)
console.log(divByThree)

// part 2
const multiplyByFive = arr.map((ele) => ele * 5)
console.log(multiplyByFive)

// part 3
const sum = arr.reduce((acc, cur) => acc + cur)
console.log(sum)