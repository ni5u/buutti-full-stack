const names = [
	"Murphy",
	"Hayden",
	"Parker",
	"Arden",
	"George",
	"Andie",
	"Ray",
	"Storm",
	"Tyler",
	"Pat",
	"Keegan",
	"Carroll"
]

const filtered = names.find((ele) => ele.length === 3 && ele[ele.length-1] === "t")

console.log(filtered)