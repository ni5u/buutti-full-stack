const competitors = ["Julia", "Mark", "Spencer", "Ann", "John", "Joe"]
const ordinals = ["st", "nd", "rd", "th"]
const output = []


for(let i = 0; i < competitors.length; i++) {
	let element = (i+1).toString()
	element += i<3?ordinals[i]:ordinals[3]
	element += " competitor was " + competitors[i]
	output.push(element)
}
// with one map()	
const outputOneMap = competitors.map((ele, index) => ((index + 1).toString() + (index<3?ordinals[index]:ordinals[3]) + " competitor was " + ele ))

console.log(output)
console.log(outputOneMap)