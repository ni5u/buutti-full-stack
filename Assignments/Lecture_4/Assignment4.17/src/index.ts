const words = process.argv[2]
const backwards = []
for(const word of words.split(" ")) {
	backwards.push(word.split("").reverse().join(""))
}
console.log(backwards.join(" "))