function sum(a: number, b:number, c = 0) {
	return a + b + c
}

const anotherSum = function (a: number, b: number, c = 0) {
	return a + b + c
}

const yetAnotherSum = (a: number, b: number, c = 0) => a + b + c

console.log("sum()")
console.log(sum(1, 2))
console.log(sum(10,20,30))
console.log("anotherSum()")
console.log(anotherSum(10,20))
console.log(anotherSum(100, 200, 300))
console.log("yetAnotherSum()")
console.log(yetAnotherSum(1000, 2000))
console.log(5,5,5)