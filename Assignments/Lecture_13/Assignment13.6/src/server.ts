import express from 'express'
import router from './forumRouter'
import { unknownEndpoint } from './middleware'

const server = express()

server.use(express.json())
server.use('/forum', router)
server.use('/', unknownEndpoint)

export default server