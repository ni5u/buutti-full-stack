import express from 'express'
import { createProductsTable } from './db'
import router from './productRouter'

const server = express()

//createProductsTable()
server.use(express.json())
server.use('/products', router)

export default server