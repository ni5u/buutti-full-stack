import { executeQuery } from "./db"

interface Product {
    name: string
    price: number
}

export const insertProduct = async (name: string, price: number) => {
    const query = 'INSERT INTO products (name, price) VALUES ($1, $2) RETURNING id, name, price;'
    const params = [name, price]
    const result = await executeQuery(query, params)
    return result.rows[0]
}

export const updateProduct = async (id: number, product: Product) => {
    let params
    let query
    if(product.name !== undefined && product.price !== undefined) {
        params = [product.name, product.price, id]
        query = 'UPDATE products SET name = $1, price = $2 WHERE id = $3 RETURNING id, name, price;'
    } else if (product.name === undefined) {
        params = [product.price, id]
        query = 'UPDATE products SET price = $1 WHERE id = $2 RETURNING id, name, price;'
    } else {
        params = [product.name, id]
        query = 'UPDATE products SET name = $1 WHERE id = $2 RETURNING id, name, price;'
    }    
    const result = await executeQuery(query, params)
    return result
}

export const getProducts = async () => {
    const query = 'SELECT * FROM products;'
    const result = await executeQuery(query)
    return result.rows
}

export const getOneProduct = async (id: number) => {
    const params = [id]
    const query = 'SELECT * FROM products WHERE id = $1;'
    const result = await executeQuery(query, params)
    console.log(result)
    return result.rows[0]
}

export const deleteProduct = async (id: number) => {
    const params = [id]
    const query = 'DELETE FROM products WHERE id = $1 RETURNING id, name, price;'
    const result = await executeQuery(query, params)
    return result
}
