import axios from 'axios';

interface Movie {
    Title: string,
    Year: number
}

async function Search(title: string, year?: number) {
  
    const url = `http://www.omdbapi.com/?apikey=c3a0092f&s=${title}&y=${year}`;
    const movies: Movie[] = [];
    
    //let movie:Movie;// = { Title:'', Year:0 };
    
    axios.get(url)
        .then(response => { // Implicit any type :(
            const data: Movie[] = response.data.Search;
            for(const index in data) {                
                const movie: Movie = data[index];
                movies.push(movie);
            }
            // Mulle jäi hämäräksi mitä tässä loopissa tehdään. Käy vertaamassa malliratkaisuun.
            movies.forEach((movie) => console.log(movie));
        })
        .catch(error => console.error(error));

}

Search('beauty',2016);