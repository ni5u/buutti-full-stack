"use strict";
// new Promise<void>((resolve) => {
//     console.log('3');
//     setTimeout(() => {
//         resolve();
//     }, 1000);
// }).then(() => {
//     return new Promise<void>((resolve) => {
//         console.log('2');
//         setTimeout(() => {
//             resolve();
//         }, 1000);
//     });
// }).then(() => {
//     return new Promise<void>((resolve) => {
//         console.log('1');
//         setTimeout(() => {
//             resolve();
//         }, 1000);
//     });
// }).then(() => console.log('go!'));
// with function
function countDown() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, 1000);
    });
}
// Testing Olli's version 
console.log(3);
countDown()
    .then(() => {
    console.log(2);
    return countDown();
})
    .then(() => {
    console.log(1);
    return countDown();
})
    .then(() => {
    console.log('GO');
    return countDown();
});
