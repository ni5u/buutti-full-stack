"use strict";
function nth(arr, nthLargest) {
    let max = 0;
    let subMax = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            subMax = max;
            max = arr[i];
        }
        if (arr[i] > subMax && arr[i] < max) {
            subMax = arr[i];
        }
    }
    return nthLargest === 1 ? max : subMax;
}
function largest(arr) {
    return nth(arr, 1);
}
function secondLargest(arr) {
    return nth(arr, 2);
}
// const numbers: number[] = [1,2,3,4,5,6];
const numbers = [4, 6, 5]; // väittää että second largest on 4! Miksi?
console.log(largest(numbers));
console.log(secondLargest(numbers));
