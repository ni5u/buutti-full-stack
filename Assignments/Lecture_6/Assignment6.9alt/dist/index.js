"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function raceLap() {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const crash = Math.random() < 0.03;
            if (crash) {
                reject('Crashed!');
            }
            else {
                const time = Math.floor(Math.random() * (25 - 20)) + 20;
                resolve(time);
            }
        });
    });
}
function race(driverNames, laps) {
    return __awaiter(this, void 0, void 0, function* () {
        const drivers = [];
        for (let i = 0; i < driverNames.length; i++) {
            drivers.push(new Driver(driverNames[i]));
        }
        for (let i = 0; i < laps; i++) {
            for (let i = 0; i < drivers.length; i++) {
                if (!drivers[i].isCrashed()) {
                    try {
                        drivers[i].addLap(yield raceLap());
                    }
                    catch (error) {
                        drivers[i].crash();
                        console.log(drivers[i].getName() + ' crashed!');
                    }
                }
            }
        }
        let fastestTotal = Infinity;
        let fastestLap = Infinity;
        let winner = new Driver('Noname');
        for (let i = 0; i < drivers.length; i++) {
            if (drivers[i].isCrashed()) {
                continue;
            }
            const totalTime = drivers[i].getTotalTime();
            if (totalTime < fastestTotal) {
                fastestTotal = totalTime;
                winner = drivers[i];
                fastestLap = drivers[i].getFastestLap();
            }
        }
        // for(let i = 0; i < drivers.length; i++) {
        //     const driver = drivers[i];
        //     console.log(`${driver.getName()} total time: ${driver.getTotalTime()} seconds. Fastest lap: ${driver.getFastestLap()} Crashed: ${driver.isCrashed()} Laps: ${driver.laps}`);
        // }
        // console.log(`Winner: ${winner.getName()} Fastest lap: ${fastestLap} Total time: ${fastestTotal}`);
        return `Winner: ${winner.getName()} Fastest lap: ${fastestLap} Total time: ${fastestTotal}`;
    });
}
class Driver {
    constructor(name) {
        this.name = name;
        this.laps = [];
        this.crashed = false;
    }
    addLap(lapTime) {
        this.laps.push(lapTime);
    }
    getName() {
        return this.name;
    }
    getFastestLap() {
        return this.laps.sort()[0];
    }
    getTotalTime() {
        return this.laps.reduce((acc, cur) => acc + cur, 0);
    }
    crash() {
        this.crashed = true;
    }
    isCrashed() {
        return this.crashed;
    }
}
(() => __awaiter(void 0, void 0, void 0, function* () { return console.log(yield race(['Kalle', 'Ville', 'Ulla'], 1)); }))();
