async function raceLap(): Promise<number> {
    return new Promise((resolve, reject) => {
        const crash = Math.random() < 0.03;

        if(crash) {
            reject('Crashed!');
        } else {
            const time = Math.floor(Math.random() * (25-20)) + 20;
            resolve(time);
        }
    });
}

async function race(driverNames: string[], laps: number) {

    const drivers: Driver[] = [];

    for(let i = 0; i < driverNames.length; i++) {
        drivers.push(new Driver(driverNames[i]));
    }
    for(let i = 0; i < laps; i++) {
        for(let i = 0; i < drivers.length; i++) {
            if(!drivers[i].isCrashed()){
                try {
                    drivers[i].addLap(await raceLap());
                } catch(error) {
                    drivers[i].crash();
                    console.log(drivers[i].getName() + ' crashed!');
                }                
            }            
        }
    }

    let fastestTotal = Infinity;
    let fastestLap = Infinity;
    let winner = new Driver('Noname');

    for(let i = 0; i < drivers.length; i++) {
        if(drivers[i].isCrashed()) {
            continue;
        }

        const totalTime = drivers[i].getTotalTime();        
        
        if( totalTime < fastestTotal) {
            fastestTotal = totalTime;
            winner = drivers[i];
            fastestLap = drivers[i].getFastestLap();
        }
    }

      
    // for(let i = 0; i < drivers.length; i++) {
    //     const driver = drivers[i];
    //     console.log(`${driver.getName()} total time: ${driver.getTotalTime()} seconds. Fastest lap: ${driver.getFastestLap()} Crashed: ${driver.isCrashed()} Laps: ${driver.laps}`);
    // }
    
    // console.log(`Winner: ${winner.getName()} Fastest lap: ${fastestLap} Total time: ${fastestTotal}`);

    return `Winner: ${winner.getName()} Fastest lap: ${fastestLap} Total time: ${fastestTotal}`;
    
}

class Driver {
    name: string;
    laps: number[];
    crashed: boolean;

    constructor(name: string) {
        this.name = name;
        this.laps = [];
        this.crashed = false;
    }

    addLap(lapTime: number) {
        this.laps.push(lapTime);
    }

    getName(): string {
        return this.name;
    }

    getFastestLap(): number {
        return this.laps.sort()[0];
    }

    getTotalTime(): number {
        return this.laps.reduce((acc, cur) => acc + cur, 0);
    }

    crash(): void {
        this.crashed = true;
    }

    isCrashed(): boolean {
        return this.crashed;
    }

}

(async ()=> console.log(await race(['Kalle', 'Ville', 'Ulla'], 1)))();

