"use strict";
const students = [
    { name: 'Markku', score: 99 },
    { name: 'Karoliina', score: 58 },
    { name: 'Susanna', score: 69 },
    { name: 'Benjamin', score: 77 },
    { name: 'Isak', score: 49 },
    { name: 'Liisa', score: 89 },
];
function scores(students) {
    const highest = students.sort((x, y) => (x.score < y.score) ? 1 : -1)[0];
    const lowest = students.sort((x, y) => (x.score > y.score) ? 1 : -1)[0];
    const average = students.reduce((acc, cur) => acc + cur.score, 0) / students.length;
    console.log(`Highest score: ${highest.name}: ${highest.score}, lowest score: ${lowest.name}: ${lowest.score}, average ${average}`);
    console.log();
    console.log('Students with above average score');
    for (const student of students) {
        if (student.score > average) {
            console.log(`${student.name}: ${student.score}`);
        }
    }
    console.log();
    console.log('Grades:');
    for (const student of students) {
        if (student.score >= 95 && student.score <= 100) {
            console.log(`${student.name}: 5`);
        }
        else if (student.score >= 80) {
            console.log(`${student.name}: 4`);
        }
        else if (student.score >= 60) {
            console.log(`${student.name}: 3`);
        }
        else if (student.score >= 40) {
            console.log(`${student.name}: 2`);
        }
        else {
            console.log(`${student.name}: 1`);
        }
    }
}
scores(students);
