const getValue = function (): Promise<number> {
    return new Promise((resolve, _reject) => {
        setTimeout(() => {
            resolve(Math.random());
        }, Math.random() * 1500);
    });
};

async function print() {
    console.log(await getValue(), await getValue());
}

print();
console.log('---');

getValue().then((val) => getValue().then((val2)=>console.log(val,val2)));