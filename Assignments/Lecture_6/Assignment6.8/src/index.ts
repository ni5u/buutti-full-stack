import axios from 'axios';


const todoUrl = 'https://jsonplaceholder.typicode.com/todos/';
const usersUrl = 'https://jsonplaceholder.typicode.com/users/';

interface Todo {
    userId: number,
    id: number,
    title: string,
    completed: boolean,
    user: User
}
interface User {
    id?: number,
    name: string,
    username: string,
    email: string,
    address?: [object],
    phone?: string,
    website?: string,
    company?: [object]
}
interface NewTodo { // sama Todo olis kelvannu tähän, kun userId on ehdollinen
    userId?: number,
    id: number,
    title: string,
    completed: boolean,
    user: User
}

// 1.
axios.get(todoUrl)
    .then(response => {
        //console.log(response.data);
    })
    .catch(error => console.error(error));


// 2. 3. 
console.log('2. 3.');

axios.get(todoUrl)
    .then(async response => {
        const todos: Todo[] = response.data;
        const newTodos: NewTodo[] = [];
        for(const todo of todos) {
            const user:User = (await axios.get(usersUrl + todo.userId)).data;
            const newTodo:NewTodo = todo;
            newTodo.user = user;
            delete newTodo.userId;
            newTodos.push(newTodo);            
        }
        console.log(newTodos);
        return newTodos;
    })
    .then((newTodos) => {
        for(const todo of newTodos) {
            delete todo.user.id;
            delete todo.user.address;
            delete todo.user.phone;
            delete todo.user.website;
            delete todo.user.company;
        }
        console.log(newTodos);
    });


// 4.
console.log('4.');
async function getTodos() {
    const todos2: Todo[] = (await axios.get(todoUrl)).data;
    const users2: User[] = (await axios.get(usersUrl)).data;
    const newTodos2: NewTodo[] = [];
    // nää on ilmeisesti kakkosia, koska ne on käytössä tuolla ylempänä? Ne on yllä omassa scopessa, joten täällä voi käyttää todos ja users uudestaan ilman sekaannusta.

    for(const todo of todos2) {        
        const newTodo:NewTodo = todo;
        newTodo.user = users2[todo.userId-1];
        delete newTodo.userId;
        newTodos2.push(newTodo);            
    }

    for(const todo of newTodos2) {
        delete todo.user.id;
        delete todo.user.address;
        delete todo.user.phone;
        delete todo.user.website;
        delete todo.user.company;
    }

    // Näitä ei lähtökohtaisesti tarvi ruveta deletoimaan. TypeScriptin näkövinkkelistä noita ei ole olemassa. Vertaa tätäkin malliratkaisuun.

    console.log(newTodos2);   
}

getTodos();