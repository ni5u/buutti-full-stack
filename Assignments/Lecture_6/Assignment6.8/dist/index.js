"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const todoUrl = 'https://jsonplaceholder.typicode.com/todos/';
const usersUrl = 'https://jsonplaceholder.typicode.com/users/';
// 1.
axios_1.default.get(todoUrl)
    .then(response => {
    //console.log(response.data);
})
    .catch(error => console.error(error));
// 2. 3. 
console.log('2. 3.');
axios_1.default.get(todoUrl)
    .then((response) => __awaiter(void 0, void 0, void 0, function* () {
    const todos = response.data;
    const newTodos = [];
    for (const todo of todos) {
        const user = (yield axios_1.default.get(usersUrl + todo.userId)).data;
        const newTodo = todo;
        newTodo.user = user;
        delete newTodo.userId;
        newTodos.push(newTodo);
    }
    console.log(newTodos);
    return newTodos;
}))
    .then((newTodos) => {
    for (const todo of newTodos) {
        delete todo.user.id;
        delete todo.user.address;
        delete todo.user.phone;
        delete todo.user.website;
        delete todo.user.company;
    }
    console.log(newTodos);
});
// 4.
console.log("4.");
function getTodos() {
    return __awaiter(this, void 0, void 0, function* () {
        const todos2 = (yield axios_1.default.get(todoUrl)).data;
        const users2 = (yield axios_1.default.get(usersUrl)).data;
        const newTodos2 = [];
        for (const todo of todos2) {
            const newTodo = todo;
            newTodo.user = users2[todo.userId - 1];
            delete newTodo.userId;
            newTodos2.push(newTodo);
        }
        for (const todo of newTodos2) {
            delete todo.user.id;
            delete todo.user.address;
            delete todo.user.phone;
            delete todo.user.website;
            delete todo.user.company;
        }
        console.log(newTodos2);
    });
}
getTodos();
