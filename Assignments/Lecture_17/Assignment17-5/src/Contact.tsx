import { useLoaderData } from 'react-router-dom'
import { contactList } from './ContactList'

export function loader({ params }: any) {
	return params.id
}

const Contact = () => {

	const contact_id = useLoaderData() as string

	const contact = contactList.find((c) => c.id === contact_id)

	return (
		<div>
			<h2>{contact&&contact.name}</h2>

			<div> <i>Phone: </i>{contact&&contact.phone}</div>
			<div><i>Email: </i>{contact&&contact.email}</div>
		</div>
	)

}

export default Contact