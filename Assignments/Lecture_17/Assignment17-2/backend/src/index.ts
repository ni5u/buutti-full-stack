import express, { Request, Response} from 'express'

const server = express()
const PORT=3000

server.use('/', express.static('./dist/client/'))

server.get('/version', (_req: Request, res: Response) => {
    res.send('Version 1.01')
})

server.listen(PORT, () => {
    console.log('Server listening port', PORT)
})