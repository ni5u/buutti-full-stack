import { Link, Outlet } from 'react-router-dom'

interface Contact {
	id: string
	name: string
	phone: string
	email: string
}

export const contactList: Contact[] = [
	{
		id: "1",
		name: "Teppo",
		phone: "32432432",
		email: "teppo@tippo.com"
	},
	{
		id: "2",
		name: "Jorma",
		phone: "35635474",
		email: "jorma@tippo.com"
	},
	{
		id: "3",
		name: "Kaisa",
		phone: "23462",
		email: "kaisa@kalia.com"
	},
	{
		id: "4",
		name: "Mannele",
		phone: "456346",
		email: "kaiza@google.com"
	}
]

const ContactList = () => {

	return (
		<div>
			<ul>
				{contactList.map((contact,i) => <li key={i}>{<Link to={contact.id}>{contact.name}</Link>}</li>)}
			</ul>
			<Outlet />
		</div>
	)

}

export default ContactList
