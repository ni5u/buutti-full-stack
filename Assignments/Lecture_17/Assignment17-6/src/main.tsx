/* eslint-disable react-refresh/only-export-components */
import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import './index.css'
import ContactList from './ContactList'
import Contact, { loader as contactLoader } from './Contact'
import ErrorPage from './ErrorPage'

const MainPage = () => {
  return (<h1>Main Page</h1>)
}

const router = createBrowserRouter([
  {
    path: '/',
    element: <MainPage />,
    errorElement: <ErrorPage />
  },
  {
    path: '/contacts/',
    element: <ContactList />,
    children: [
      {
        path: ':id',
        element: <Contact />,
        loader: contactLoader,
        errorElement: <ErrorPage />
      }
    ]

  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
)
