import { useState } from 'react'
import { CSSProperties } from 'react'




function App() {

  function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  const [randomColor, setRandomColor] = useState(getRandomColor())

  const boxStyle: CSSProperties = {
    backgroundColor: randomColor,
    width: '200px',
    height: '200px',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  }

  const hexStyle: CSSProperties = {
    color: 'black',
    position: 'absolute',
    top: '35%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  }

  return (
    <>
      <h1>Inline css</h1>
      <div className='container'>
        <div style={hexStyle}>{randomColor}</div>
        <div style={boxStyle} onClick={() => setRandomColor(getRandomColor())} className="colorBox"> </div>
      </div>
    </>
  )
}

export default App
