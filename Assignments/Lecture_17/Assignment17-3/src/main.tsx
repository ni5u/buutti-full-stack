/* eslint-disable react-refresh/only-export-components */
import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import './index.css'


const MainPage = () => {
  return (<h1>Main Page</h1>)
}

const Contacts = () => {
  return <div>Contacts!</div>
}

const router = createBrowserRouter([
    {
        path: '/',
        element: <MainPage />,
    },
    {
      path: '/contacts',
      element: <Contacts />
  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
