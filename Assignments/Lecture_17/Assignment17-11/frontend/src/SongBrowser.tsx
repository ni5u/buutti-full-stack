import { useState,  ChangeEvent, useEffect } from 'react'
import './SongBrowser.css'
import { Link, Outlet } from 'react-router-dom'
import axios from 'axios'

interface Song {
	id: number
	title: string
	lyrics: string
}

function SongBrowser() {
	const [songFilter, setSongFilter] = useState('')
	const [songs, setSongs] = useState<Song[]>([])

	const handleSongFilter = (event: ChangeEvent<HTMLInputElement>) => {
		setSongFilter(event.target.value)
	}

	useEffect(() => {
		const getSongs = async () => {		
			const response = await axios.get('http://localhost:3000/songs')

			setSongs(response.data)
		}

		getSongs()

	},[])


	let songList

	if (songFilter.length > 0) {
		const filteredSongs = songs.filter((song) => song.title.toLowerCase().includes(songFilter))
		songList = filteredSongs
	} else {
		songList = songs
	}
	
	const songNavigation = songList.map((song) => <li key={'songs' + song.id}>{<Link to={song.id.toString()}>{song.title}</Link>}</li>)

	return (
		<>
			<h1>Song Book</h1>
			<div className="songBrowser">
				<div className='leftColumn'>
					<input onChange={handleSongFilter} type='text'></input>
					<ul className='songList'>
						{songNavigation}
					</ul>
				</div>
				<div className='songInfo'>
					<Outlet />
				</div>

			</div>


		</>
	)
}

export default SongBrowser
