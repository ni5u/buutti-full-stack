// db.ts
import pg from 'pg'
// import songs from './songs'

interface Song {
    id: number
    title: string
    lyrics: string
}

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, NODE_ENV } = process.env

console.log('PG-VARIABLES', { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, NODE_ENV })

const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: NODE_ENV === 'production'
})

export const executeQuery = async (query: string, parameters?: Array<any>) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result
    } catch (error: any) {
        console.error(error.stack)
        error.name = 'dbError'
        throw error
    } finally {
        client.release()
    }
}

// export const createTable = async () => {
//     const query = 'CREATE TABLE songs ( id SERIAL PRIMARY KEY, title VARCHAR NOT NULL, lyrics VARCHAR(2000) NOT NULL);'
//     const result = await executeQuery(query)
// }

// export const addSong = async (title: string, lyrics: string) => {
//     const params = [title, lyrics.replace(/\n/g,'\n')]
//     const query = 'INSERT INTO songs (title, lyrics) VALUES ($1, $2) RETURNING lyrics;'
    
//     const result = await executeQuery(query, params)
//     return result.rows
// }

// export const addSongs = async () => {
//     for(const song of songs) {
//         const result = await addSong(song.title, song.lyrics)
//         console.log('result: ' + result)
//     }    
 
// }
