import { Router, Request, Response } from 'express'
import {  getSongs, getSong } from './dao'

interface Song {
    id: number
    title: string
    lyrics: string
}

const router = Router()

router.get('/', async (req: Request, res: Response) => {
    const result = await getSongs()
    res.status(200).send(result)
})

router.get('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const song: Song = await getSong(id)
    res.status(200).send(song)
})

export default router