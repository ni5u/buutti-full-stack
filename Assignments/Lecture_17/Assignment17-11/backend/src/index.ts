import server from './server'
import { executeQuery,/* createTable, addSongs*/ } from './db'

const PORT = 3000

// const prepareDatabase = async () => {
//     await createTable()
//     addSongs()
// }
// prepareDatabase()

server.listen(PORT, () => {
    console.log('Server listening port', PORT)
})