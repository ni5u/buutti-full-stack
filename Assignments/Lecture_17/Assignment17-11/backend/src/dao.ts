import { executeQuery } from "./db"

interface Song {
    id: number
    title: string
    lyrics: string
}

export const getSongs = async () => {
    const query = 'SELECT id, title FROM songs ORDER BY id;'
    const result = await executeQuery(query)
    return result.rows
}

export const getSong = async (id: number) => {
    const params = [id]
    const query = 'SELECT * FROM songs WHERE id = $1;'
    const result = await executeQuery(query, params)
    const song: Song = result.rows[0]
    return song
}
