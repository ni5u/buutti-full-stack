import express, { Request, Response } from 'express'
import router from './songRouter'
import { songs } from './songs'

const server = express()

server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  })

server.use('/', express.static('./dist/client/'))
server.use('/songs', router)

server.get('/version', (_req: Request, res: Response) => {
    res.send('Version 1.1')
})

// server.get('/songs', (_req: Request, res: Response) => {
//     const songList = songs.map((song) => {
//         return { id: song.id, title: song.title }
//     })
//     res.send(songList)
// })

// server.get('/songs/:id', (req: Request, res: Response) => {
//     const song = songs.find((song) => song.id === Number(req.params.id))

//     res.send(song)
// })

// Redirect all other requests to React Router
server.get('*', (req, res) => {
    res.sendFile('index.html', { root: './dist/client' })
})

export default server