import axios from 'axios'
import { useState, useEffect } from 'react'

const Version = () => {

    const [versio, setVersio] = useState('')

    useEffect(() => {

        const getVersio = async () => {
            const version = (await axios.get('http://localhost:3000/version')).data
            console.log(version)
            setVersio(version)
        }
        getVersio()

    },[])

    return (<h1>{versio}</h1>)


}

export default Version