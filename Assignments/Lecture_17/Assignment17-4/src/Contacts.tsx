import { useLoaderData } from 'react-router-dom'


const contactList = [
	{
		id: "1",
		name: "Teppo",
		phone: "32432432",
		email: "teppo@tippo.com"
	},
	{
		id: "2",
		name: "Jorma",
		phone: "35635474",
		email: "jorma@tippo.com"
	}
]


const Contacts = () => {

	const id = useLoaderData() as string

	const dude = contactList.find((c) => c.id === id)

	return (
		<div>
			<h2>{dude && dude.name}</h2>

			<div> <i>Phone: </i>{dude && dude.phone}</div>
			<div><i>Email: </i>{dude && dude.email}</div>
		</div>
	)

}

export function loader({ params }: any) {
	return params.id
}

export default Contacts
