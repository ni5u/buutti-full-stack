export interface Song {
    id: number
    title: string
    lyrics: string
}

export const songs: Song[]
