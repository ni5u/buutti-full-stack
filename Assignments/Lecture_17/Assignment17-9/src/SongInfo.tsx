import { useLoaderData } from 'react-router-dom'
import { songs } from './songs.js'

export function loader({ params }: any) {
    const songIds = songs.map((song) => song.id.toString())
    if (songIds.includes(params.id)) {
        
        return params.id
    }
        

    throw new Response('Wrong id', {status:404})
}


const SongInfo = () => {
    const song_id = useLoaderData() as string

    const currentSong = songs.find((song) => song.id.toString() === song_id)

    return (
        <div>
            <h2>{currentSong && currentSong.title}</h2>
            <p>{currentSong && currentSong.lyrics}</p>
        </div>
    )

}

export default SongInfo