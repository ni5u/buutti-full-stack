import { useLoaderData } from 'react-router-dom'
import { useEffect, useState } from 'react'
import axios from 'axios'

interface Song {
    id: number
    title: string
    lyrics: string
}

export function loader({ params }: any) {

    return params.id
    // throw new Response('Wrong id', { status: 404 })
}


const SongInfo = () => {
    const song_id = useLoaderData() as string
    const [currentSong, setCurrentSong] = useState<Song>({id:0,title:'',lyrics:''})

    useEffect(() => {
        const getSongInfo = async () => {

            const response = await axios.get('http://localhost:3000/songs/' + song_id)
            setCurrentSong((await response).data)
        }
        
        getSongInfo()

    },[song_id])
    


    return (
        <div>
            <h2>{currentSong && currentSong.title}</h2>
            <p>{currentSong && currentSong.lyrics}</p>
        </div>
    )

}

export default SongInfo