import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import SongBrowser from './SongBrowser'
import SongInfo, { loader as songLoader } from './SongInfo'
import ErrorPage from './ErrorPage'

const router = createBrowserRouter([
    {
        path: '/app/songs/',
        element: <SongBrowser />,
        errorElement: <ErrorPage />,
        children: [
          {
            path: ':id',
            element: <SongInfo />,
            loader: songLoader,
            errorElement: <ErrorPage />
          }
        ]
    }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
