const originalChar = process.argv[2]
const newChar = process.argv[3]
const text = process.argv[4]

const re = new RegExp(originalChar,"g")
// Regexejä, next level! Muista myös .replaceAll()
console.log(text.replace(re,newChar))

// Näyttää siltä että on helpolla menneet nämä tehtävät sulta. Kompaktia ja selkeetä koodia. Jatka samaan malliin.