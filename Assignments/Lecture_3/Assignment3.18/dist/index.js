"use strict";
const originalChar = process.argv[2];
const newChar = process.argv[3];
const text = process.argv[4];
const re = new RegExp(originalChar, "g");
console.log(text.replace(re, newChar));
