"use strict";
const n = 17;
let ind = 1;
let sum = 0;
for (let i = 1; i <= n; i++) {
    if (i % 3 == 0 || i % 5 == 0) {
        sum += i;
    }
}
console.log(sum);
sum = 0;
while (ind <= n) {
    if (ind % 3 == 0 || ind % 5 == 0) {
        sum += ind;
    }
    ++ind;
}
console.log(sum);
