for (let i = 1; i <= 100; i++) {
    let fizzBuzz = ""
    
    if (i % 3 === 0) {
        fizzBuzz += "Fizz"
    }
    if (i % 5 === 0) {
        fizzBuzz += "Buzz"
    }
    if(fizzBuzz.length > 0) {
        console.log(fizzBuzz)
    } else {
        console.log(i)
    }
}