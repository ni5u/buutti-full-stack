let balance
const isActive = true
const checkBalance = false
balance = 100

if(checkBalance) {
    if(isActive && balance > 0) {
        console.log("Your balance is " + balance)
    } else if(!isActive) {
        console.log("Your acccount is not active.")
    } else if(balance === 0) {
        console.log("Your account is empty.")
    } else {
        console.log("Your balance is negative.")
    }
} else {
    console.log("Have a nice day!")
}