"use strict";
const names = process.argv.slice(2);
//Initial letters
let initials = "";
for (const name of names) {
    initials += name[0];
    initials += ".";
}
console.log(initials.slice(0, -1));
names.sort((a, b) => {
    return b.length - a.length;
});
for (const name of names) {
    process.stdout.write(name + " ");
}
