"use strict";
for (let i = 0; i <= 1000; i += 100) {
    process.stdout.write(i.toString() + " ");
}
console.log();
for (let i = 1; i <= 128; i *= 2) {
    process.stdout.write(i.toString() + " ");
}
console.log();
for (let i = 3; i <= 15; i += 3) {
    process.stdout.write(i.toString() + " ");
}
console.log();
for (let i = 9; i >= 0; i--) {
    process.stdout.write(i.toString() + " ");
}
console.log();
for (let i = 1; i <= 4; i++) {
    for (let j = 0; j < 3; j++) {
        process.stdout.write(i.toString() + " ");
    }
}
console.log();
for (let i = 1; i <= 3; i++) {
    for (let j = 0; j <= 4; j++) {
        process.stdout.write(j.toString() + " ");
    }
}
