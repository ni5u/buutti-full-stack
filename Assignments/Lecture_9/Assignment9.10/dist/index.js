"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const books = [];
const server = (0, express_1.default)(); // server is of type "Express"
server.use(express_1.default.json());
// Remove debug responds when everything is working! Also 8.11
server.post('/api/v1/books', (req, res) => {
    const book = {
        'id': req.body.id,
        'name': req.body.name,
        'author': req.body.author,
        'read': req.body.read
    };
    books.push(book);
    res.status(200).send(books);
});
server.put('/api/v1/books/:id', (req, res) => {
    const id = Number(req.params.id);
    const book = books.find((book) => book.id === id);
    if (book) {
        const name = req.body.name;
        const author = req.body.author;
        const read = req.body.read;
        book['name'] = name ? name : book['name'];
        book['author'] = author ? author : book['author'];
        book['read'] = read ? read : book['read'];
        res.status(204).send(book);
        return;
    }
    res.status(404).send('Book not found!');
});
server.get('/api/v1/books', (req, res) => {
    res.status(200).send(books);
});
server.get('/api/v1/books/:id', (req, res) => {
    const id = Number(req.params.id);
    for (const book of books) {
        if (book.id === id) {
            res.status(200).send(book);
            return;
        }
    }
    res.status(404).send('Book not found!');
});
server.delete('/api/v1/books/:id', (req, res) => {
    const id = Number(req.params.id);
    for (const book of books) {
        if (book.id === id) {
            books.splice(books.indexOf(book), 1);
            res.status(204).send(book);
            return;
        }
    }
    res.status(404).send('Book not found!');
});
server.listen(3000, () => {
    console.log('Listening to port 3000');
});
