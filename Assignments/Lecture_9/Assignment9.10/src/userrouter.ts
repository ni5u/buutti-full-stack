import express, { Request, Response } from 'express'
import argon from 'argon2'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const userRouter = express.Router()
const users: { 'username': string, 'hash': string }[] = []
const secret = process.env.SECRET ?? ''

userRouter.post('/register', (req: Request, res: Response) => {
    const { username, password } = req.body
    if ((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    if (users.find((user) => user.username === username)) {
        res.status(409).send('Username exists')
        return
    }
    argon.hash(password)
        .then((result) => {
            users.push({ username: username, hash: result })
            const token = jwt.sign(username, secret)
            res.status(201).send(token)
        })
})

userRouter.post('/login', (req: Request, res: Response) => {

    const { username, password } = req.body
    if ((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    const user = users.find((user) => user.username === username)
    if (user) {
        argon.verify(user.hash, password)
            .then((result) => {
                if (result) {
                    const token = jwt.sign(username, secret)
                    res.status(200).send(token)
                } else {
                    res.status(401).send()
                }
            })
    } else {
        res.status(401).send('No such user')
    }

})

export default userRouter