import express, { Request, Response} from 'express'
import {validator} from './middleware'
import jwt from 'jsonwebtoken'

interface Book {
    'id': number,
    'name': string,
    'author': string,
    'read': boolean
}

interface CustomRequest extends Request {
    user?: jwt.JwtPayload | string
}

const books: Book[] = []

const bookRouter = express.Router()

bookRouter.post('/', validator)
bookRouter.post('/', (req: CustomRequest, res: Response) => {
    const book: Book = {
        'id': req.body.id,
        'name': req.body.name,
        'author': req.body.author,
        'read': req.body.read
    }
    books.push(book)
    
    res.status(200).send()
})
bookRouter.put('/:id', validator)
bookRouter.put('/:id', (req: CustomRequest, res: Response) => {
    const id = Number(req.params.id)
    const book = books.find((book)=> book.id === id)
    if(book) {
        const name = req.body.name
        const author = req.body.author
        const read = req.body.read
        book['name'] = name?name:book['name']
        book['author'] = author?author:book['author']
        book['read'] = read!==undefined?read:book['read']
        res.status(204).send()
        return
    }        
    res.status(404).send('Book not found!')
})

bookRouter.get('/', (req: Request, res: Response) => {
    res.status(200).send(books)
})

bookRouter.get('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    for(const book of books) {
        if(book.id === id) {
            res.status(200).send(book)
            return
        }
    }    
    res.status(404).send('Book not found!')
})

bookRouter.delete('/:id', (req: CustomRequest, res: Response) => {
    const id = Number(req.params.id)
    for(const book of books) {
        if(book.id === id) {
            books.splice(books.indexOf(book),1)
            res.status(204).send(book)
            return
        }
    }    
    res.status(404).send('Book not found!')
})

export default bookRouter