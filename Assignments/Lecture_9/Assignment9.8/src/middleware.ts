import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import 'dotenv/config'

interface CustomRequest extends Request {
    user?: jwt.JwtPayload | string
}

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Page not found!'})
}

export const validatePost =  (req: Request, res: Response, next: NextFunction) => {
    if(!('id' in req.body &&'name' in req.body && 'email' in req.body)) {
        res.status(400).send('Missing parameters: Use: id, name, email')
    } else {
        next()
    }
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
    
    const time = new Date()
    const logData = {
        time: time,
        requestMethod: req.method,
        url: req.url,
        body: req.body
    }
    const output = `${logData.time}: ${logData.requestMethod}, ${logData.url}, ${JSON.stringify(logData.body)}`
    console.log(output)
    next()
}

export const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
    const auth = req.get('Authorization')
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('Invalid token')
    }

    const token = auth.substring(7)
    const secret = process.env.SECRET??''
    try {
        const decodedToken = jwt.verify(token, secret)
        req.user = decodedToken
        next()
    } catch (error) {
        return res.status(401).send('Invalid token')
    }
}

