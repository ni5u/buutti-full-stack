import express from 'express'
import {authenticate, logger, unknownEndpoint} from './middleware'
import studentRouter from './studentRouter'
import userRouter from './userRouter'


const server = express() 
server.use(express.json())
server.use(logger)
server.use('/', express.static('public'))
server.use('/student',authenticate)
server.use('/student', studentRouter)
server.use('/', userRouter)
server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})