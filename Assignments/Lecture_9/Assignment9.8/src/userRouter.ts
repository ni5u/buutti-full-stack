import express, { Request, Response } from 'express'
import argon from 'argon2'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const userRouter = express.Router()
const users: { 'username': string, 'hash': string }[] = []
const secret = process.env.SECRET ?? ''
const admin = process.env.ADMIN
const adminpwd = process.env.PASSWORD // 'Salasana' // tää on silti oletettavasti hash, vaikka muuttujan nimi on password?

userRouter.post('/register', (req: Request, res: Response) => {
    const { username, password } = req.body
    if ((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    argon.hash(password)
        .then((result) => {
            users.push({ username: username, hash: result })
            res.status(201).send()
            // register ei lähetä tokenia :(
        })
})

userRouter.post('/login', (req: Request, res: Response) => {
    const { username, password } = req.body
    if ((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    const user = users.find((user) => user.username === username)
    if (user) {
        argon.verify(user.hash, password)
            .then((result) => {
                if (result) {
                    const token = jwt.sign(username, secret)
                    res.status(200).send(token)
                } else {
                    res.status(401).send()
                }
            })
    } else {
        res.status(401).send('No such user')
    }

})

userRouter.post('/admin', (req: Request, res: Response) => {
    const { username, password } = req.body
    if (!(username && password)) {
        return res.status(401).send()
    }

    if (username === admin) {
        argon.verify(adminpwd ?? '', password)
            .then((result) => {
                if (result) {
                    return res.status(204).send()
                }
                return res.status(401).send()
            })
    }
    res.status(401).send()
})



export default userRouter