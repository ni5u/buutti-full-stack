import 'dotenv/config'
import jwt from 'jsonwebtoken'

const secret = process.env.SECRET??''

const token = jwt.verify(process.argv[2], secret)
console.log(token)