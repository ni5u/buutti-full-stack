import express from 'express'
import {logger, unknownEndpoint} from './middleware'
import router from './studentRouter'

const server = express() // server is of type "Express"

server.use(express.json())
server.use(logger)
server.use('/', express.static('public'))
server.use('/student', router)
server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})