import express, { Request, Response} from 'express'
import { validatePost} from './middleware'

const studentRouter = express.Router() 
const students: {'id': number, 'name': string,'email': string }[] = [] // käytä interfacea

studentRouter.get('/', (_req: Request, res: Response) => {
    const studentIDs = students.map((student) => student.id)
    res.send(studentIDs)   
})

studentRouter.get('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    }    
    res.status(200).send(student)   
})

studentRouter.put('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    } else {        
        if('name' in req.body) {
            student.name = req.body.name
        }
        if('email' in req.body) {
            student.email = req.body.email
        }
        if(!('name' in req.body || 'email' in req.body)) {
            res.status(400).send('No name or email')
        }
    }    
    res.status(204).send(student)   
})

studentRouter.post('/',validatePost)
studentRouter.post('/', (req: Request, res: Response) => {
    const id = Number(req.body.id)
    const name = req.body.name
    const email = req.body.email
    if(students.find((student) => student.id === id)) {
        res.status(400).send('Duplicate id!')
    } else {
        students.push({'id': id, 'name': name, 'email': email})
        res.status(201).send()
    }        
})

studentRouter.delete('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    } else {
        students.splice(students.indexOf(student))
        res.status(204).send(student)
    }      
})

export default studentRouter