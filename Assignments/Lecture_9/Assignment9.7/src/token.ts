import 'dotenv/config'
import jwt from 'jsonwebtoken'

const payload = { username: 'nisu' }
const secret = process.env.SECRET??''
const options = { expiresIn: '1h'}

const token = jwt.sign(payload, secret, options)
console.log(token)