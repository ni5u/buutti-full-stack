import express, { Request, Response} from 'express'
import argon from 'argon2'

const userRouter = express.Router() 
const users: {'username': string, 'hash': string }[] = []


userRouter.post('/register', (req: Request, res: Response) => {
    const {username, password} = req.body
    if((!username && password)) { // tämä ehtolause sanoo "jos käyttäjätunnus puuttuu ja salasana löytyy", ehkä sulku väärässä paikassa?
        res.status(400).send('Need username and password')
        return
    }
    argon.hash(password)
        .then((result: string) => { // implicit any
            users.push({username: username, hash: result})
            res.status(201).send()         
        })    
})



export default userRouter