import express, { Request, Response } from 'express'
import argon from 'argon2'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const userRouter = express.Router()
const users: { 'username': string, 'hash': string }[] = []
const secret = process.env.SECRET ?? ''
const adminPWD = process.env.ADMINPWD ?? ''

users.push({ 'username': 'admin', 'hash': adminPWD })

userRouter.post('/register', (req: Request, res: Response) => {
    const { username, password } = req.body
    if ((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    if (users.find((user) => user.username === username)) {
        res.status(409).send('Username exists')
        return
    }
    argon.hash(password)
        .then((result) => {
            users.push({ username: username, hash: result })
            const token: { username: string, isAdmin?: boolean } = { username }
            if (username === 'admin') {
                token.isAdmin = true
            }
            // ^ mitäs tää täällä tekee? jos username on admin, niin toi edellinen if-blokki on heittäny meidät jo 409:llä pihalle
            const encryptedToken = jwt.sign(token, secret)
            res.status(201).send(encryptedToken)
        })
})

userRouter.post('/login', (req: Request, res: Response) => {

    const { username, password } = req.body
    if ((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    const user = users.find((user) => user.username === username)
    if (user) {
        argon.verify(user.hash, password)
            .then((result) => {
                if (result) {
                    const token: { username: string, isAdmin?: boolean } = { username } // tää on payload, ei token
                    if (username === 'admin') {
                        token.isAdmin = true
                    }
                    // ehkä vaan
                    // const payload = { username, isAdmin: username === 'admin' }            
                    const encryptedToken = jwt.sign(token, secret)
                    res.status(200).send(encryptedToken)
                } else {
                    res.status(401).send()
                }
            })
    } else {
        res.status(401).send('No such user')
    }

})

export default userRouter
// laittaisin muuten myös tiedostojen nimet camelCasella