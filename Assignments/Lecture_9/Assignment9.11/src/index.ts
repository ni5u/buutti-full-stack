import express from 'express'
import { unknownEndpoint, logger, authenticate, userIsAdmin } from './middleware'
import bookRouter from './bookrouter'
import helmet from 'helmet'
import userRouter from './userrouter'

const server = express() 

server.use(express.json())
server.use(helmet())
server.use(logger)
server.use('/api/v1/users', userRouter)
server.use('/api/v1/books', authenticate, userIsAdmin)
server.use('/api/v1/books', bookRouter)
server.use(unknownEndpoint)


server.listen(3000, () => {
    console.log('Listening to port 3000')
})