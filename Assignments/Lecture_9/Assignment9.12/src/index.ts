import server from './server'

server.listen(3000, () => {
    console.log('Listening to port 3000')
})

/*
Hyvin näyttää sujuvan. Aika monessa tiedostossa oli tosi tiivis formatointi, joka ei vastaa JS/TS standardeja. Nää voi tietty tiimin kanssa sopia erikseen, mutta kannattaa ottaa tavaksi komentaa VSC:tä formatoimaan koodi Shift-Alt-F näppäinyhdistelmällä, niin tulee normitettua välitystä koodeihin.
*/