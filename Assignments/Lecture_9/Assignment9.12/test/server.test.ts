import request from 'supertest'
import server from '../src/server'

test('dummy test', () => {
    expect(true).toBe(true)
})

test('404 on invalid address', async () => {
    const response = await request(server)
        .get('/invalidaddress')
    expect(response.statusCode).toBe(404)
})

test('Register needs username and password', async () => {
    const response = await request(server)
        .post('/api/v1/users/register')
        .send({})        
    expect(response.statusCode).toBe(400)
})

test('Login: no such user', async () => {
    const response = await request(server)
        .post('/api/v1/users/login')
        .send({'username': 'nisu', 
            'password': 'pipeo'})            
    expect(response.statusCode).toBe(401)
})

test('valid registration', async () => {
    const response = await request(server)
        .post('/api/v1/users/register')
        .send({'username': 'nisu', 
            'password': 'pipeo'})        
    expect(response.statusCode).toBe(201)
})

test('username exists', async () => {
    await request(server)
        .post('/api/v1/users/register')
        .send({'username': 'nisu', 
            'password': 'pipeo'})
    const response = await request(server)
        .post('/api/v1/users/register')
        .send({'username': 'nisu', 
            'password': 'pipeo'})            
    expect(response.statusCode).toBe(409)
})

test('valid login', async () => {
    // await request(server)                // apparently nisu is still registered
    //     .post('/api/v1/users/register')
    //     .send({'username': 'nisu', 
    //         'password': 'pipeo'})
    const response = await request(server)
        .post('/api/v1/users/login')
        .send({'username': 'nisu', 
            'password': 'pipeo'})
    expect(response.statusCode).toBe(200)
})

test('Get book. Book not found!', async () => {
    let response = await request(server)
        .post('/api/v1/users/login')
        .send({'username': 'nisu', 
            'password': 'pipeo'})     
    const TOKEN = response.text
    response = await request(server)
        .get('/api/v1/books/1')
        .set('Authorization', `Bearer ${TOKEN}`)
    expect(response.statusCode).toBe(404)
})

test('Get book. Invalid token', async () => {
    const TOKEN = ''
    const response = await request(server)
        .get('/api/v1/books/1')
        .set('Authorization', `Beare ${TOKEN}`)
    expect(response.statusCode).toBe(401)
})

test('Add book as nisu', async () => {
    let response = await request(server)
        .post('/api/v1/users/login')
        .send({'username': 'nisu', 
            'password': 'pipeo'})     
    const TOKEN = response.text
    response = await request(server)
        .post('/api/v1/books/')
        .send({            
            'id': 1,
            'name': 'Sepox',
            'author': 'Teppo meni saunaan',
            'read': false            
        })
        .set('Authorization', `Bearer ${TOKEN}`)
    expect(response.statusCode).toBe(403)
})

test('Add book as admin', async () => {
    let response = await request(server)
        .post('/api/v1/users/login')
        .send({'username': 'admin', 
            'password': 'Ive made a huge mistake'})     
    const TOKEN = response.text
    response = await request(server)
        .post('/api/v1/books/')
        .send({            
            'id': 1,
            'name': 'Sepox',
            'author': 'Teppo meni saunaan',
            'read': false            
        })
        .set('Authorization', `Bearer ${TOKEN}`)
    expect(response.statusCode).toBe(200)
})

test('Get books as nisu', async () => {
    let response = await request(server)
        .post('/api/v1/users/login')
        .send({'username': 'nisu', 
            'password': 'pipeo'})     
    const TOKEN = response.text
    response = await request(server)
        .get('/api/v1/books/')
        .set('Authorization', `Bearer ${TOKEN}`)
    expect(response.statusCode).toBe(200)
    expect(response.body).toEqual([ { id: 1, name: 'Sepox', author: 'Teppo meni saunaan', read: false } ])
})

// testaamisen haasteellisin puoli on nimenomaan siinä, että testit ajetaan rinnakkain, joten ei voida tietää mitkä muuttujat on missäkin tilassa, koska testien järjestystä ei taata. kannattaa tutustua beforeEach ja beforeAll funktioihin

