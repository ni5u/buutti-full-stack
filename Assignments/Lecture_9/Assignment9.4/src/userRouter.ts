import express, { Request, Response} from 'express'
import argon from 'argon2'

const userRouter = express.Router() 
const users: {'username': string, 'hash': string }[] = []


userRouter.post('/register', (req: Request, res: Response) => {
    const {username, password} = req.body
    if((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    argon.hash(password)
        .then((result) => {
            users.push({username: username, hash: result})
            res.status(201).send()         
        })    
})

userRouter.post('/login', (req: Request, res: Response) => {
    const {username, password} = req.body
    if((!username && password)) {
        res.status(400).send('Need username and password')
        return
    }
    const user = users.find((user) => user.username === username)
    if(user) {
        argon.verify(user.hash,password)
            .then((result) => { // result on aika vähän kuvaava nimi
                if(result) {
                    res.status(204).send()
                } else {
                    res.status(401).send()
                }                 
            }) 
            // .then((isCorrectPassword: boolean) => res.status(isCorrectPassword ? 204 : 401).send())a
    } else {
        res.status(401).send('No such user')
    }
    
}) 



export default userRouter