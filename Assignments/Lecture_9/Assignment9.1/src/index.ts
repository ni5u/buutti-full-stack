import express, { Request, Response} from 'express'
import {logger, unknownEndpoint, validatePost} from './middleware'

const server = express() // server is of type "Express"
const students: {'id': number, 'name': string,'email': string }[] = []

server.use(express.json())
server.use(logger)

server.use('/', express.static('public'))

server.get('/students', (_req: Request, res: Response) => {
    const studentIDs = students.map((student) => student.id)
    res.send(studentIDs)   
})

server.get('/student/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    }    
    res.status(200).send(student)   
})

server.put('/student/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    } else {        
        if('name' in req.body) {
            student.name = req.body.name
        }
        if('email' in req.body) {
            student.email = req.body.email
        }
        if(!('name' in req.body || 'email' in req.body)) {
            res.status(400).send('No name or email')
        }
    }    
    res.status(204).send(student)   
})

server.post('/student',validatePost)
server.post('/student', (req: Request, res: Response) => {
    const id = Number(req.body.id)
    const name = req.body.name
    const email = req.body.email
    if(students.find((student) => student.id === id)) {
        res.status(400).send('Duplicate id!')
    } else {
        students.push({'id': id, 'name': name, 'email': email})
        res.status(201).send()
    }        
})

server.delete('/student/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    } else {
        students.splice(students.indexOf(student))
        res.status(204).send(student)
    }      
})


server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})