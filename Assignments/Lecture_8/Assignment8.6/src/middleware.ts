import { Request, Response, NextFunction } from 'express'
import fs from 'fs'

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Page not found!'})
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
    
    const logPath = 'students.log'
    let log = fs.readFileSync(logPath, 'utf-8')
    console.log(new Date(), req.method, req.originalUrl)
    const output = new Date() + ', ' + req.method + ', ' + req.originalUrl + '\n'
    log += output 
    fs.writeFileSync(logPath, log)
    next()
}
