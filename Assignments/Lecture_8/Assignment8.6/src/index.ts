import express, { Request, Response} from 'express'
import {logger, unknownEndpoint} from './middleware'

const server = express() // server is of type "Express"

server.use(logger)

server.get('/students', (_req: Request, res: Response) => {

    res.send([])   
})

server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})