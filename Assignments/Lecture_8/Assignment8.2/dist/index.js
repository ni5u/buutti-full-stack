"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server = (0, express_1.default)(); // server is of type "Express"
server.listen(3000, () => {
    console.log('Listening to port 3000');
});
server.get('/', (_req, res) => {
    res.send('Hello world!');
});
