import { Request, Response, NextFunction } from 'express'

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Nothing here!'})
}

export const validator =  (req: Request, res: Response, next: NextFunction) => {
    const id = req.body.id
    const name = req.body.name
    const author = req.body.author
    const read = req.body.read
    if(req.method === 'POST') {
        if(!('id' in req.body && 'name' in req.body && 'author' in req.body && 'read' in req.body)) {
            res.status(400).send('Missing parameters: Use: id: number, name: string, author: string, read: boolean')
            return
        }
        if(typeof id !== 'number' || typeof name !== 'string' || typeof author !== 'string' || typeof read !== 'boolean') {
            res.status(400).send('Invalid parameter type: Use: id: number, name: string, author: string, read: boolean')
            return
        }
    } else if(req.method === 'PUT') {
        if(!('name' in req.body || 'author' in req.body || 'read' in req.body)) {  
            res.status(400).send('Missing parameters. Must have at least one of: name, author, read')
            return
        }
        if((typeof typeof name !== 'string' && typeof name !== 'undefined')
        || (typeof author !== 'string' && typeof author !== 'undefined')
        || (typeof read !== 'boolean' && typeof read !== 'undefined')) {
            res.status(400).send('Invalid parameter type: Use: name: string, author: string, read: boolean')
            return
        } 
    }
    next()
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
    
    const time = new Date()
    const logData = {
        time: time,
        requestMethod: req.method,
        url: req.url,
        body: req.body
    }
    const output = `${logData.time}: ${logData.requestMethod}, ${logData.url}, ${JSON.stringify(logData.body)}`
    console.log(output)
    next()
}
