import express, { Request, Response, NextFunction } from 'express'
import fs, { readFileSync } from 'fs'

const server = express() // server is of type "Express"
const logPath = 'students.log'

const logger = (req: Request, res: Response, next: NextFunction) => {
    
    let log = readFileSync(logPath, 'utf-8')
    console.log(new Date(), req.method, req.originalUrl)
    const output = new Date() + ', ' + req.method + ', ' + req.originalUrl + '\n'
    log += output 
    fs.writeFileSync(logPath, log)
    next()
}


server.use(logger)

server.get('/students', (_req: Request, res: Response) => {

    res.send([])   
})



server.listen(3000, () => {
    console.log('Listening to port 3000')
})