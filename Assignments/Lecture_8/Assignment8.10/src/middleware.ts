import { Request, Response, NextFunction } from 'express'

export interface Student  {'name': string,'score': number }

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Page not found!'})
}

export const validator =  (req: Request, res: Response, next: NextFunction) => {
    try {
        if(!('students' in req.body)) {
            res.status(400).send('Missing student array!')
        }
        const students: Student[] = req.body.students
        for(const student of students) {
            const name = student.name
            const score = student.score
            if(!name || !score) {
                res.status(400).send('Invalid input!')
            }
        }
    } catch {
        res.status(400).send('Invalid input!')
    }
    
    next()
     
}