import express, { Request, Response} from 'express'
import {unknownEndpoint, validator, Student} from './middleware'

const server = express() // server is of type "Express"

server.use(express.json())

server.post('/checkscores',validator)
server.post('/checkScores', (req: Request, res: Response) => {
    const students: Student[] = req.body.students

    const average = students.reduce((acc, cur) => acc + cur.score, 0) / students.length
    const topScore = students.reduce((acc, cur) => acc>cur.score?acc:cur.score,0)
    const bestStudent = students.find((student) => student.score === topScore)
    const aboveAverage = students.filter((student) => student.score >= average)
    const output = {
        'Average score': average,
        'Above average': students.length / aboveAverage.length,
        'Highest score': bestStudent
    }
    res.status(200).send(output)       
})

server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})