import express, { Request, Response } from 'express'

const server = express() // server is of type "Express"
const names: { [key: string]: number } = {}

server.get('/counter/:name', (_req: Request, res: Response) => {
    const name = _req.params.name
    if(name in names) {
        names[name]++
    } else {
        names[name] = 1
    }
    res.send(`${name} was here ${names[name]} times`)   
})

server.get('/counter/', (_req: Request, res: Response) => {
    res.send(names)
})


server.listen(3000, () => {
    console.log('Listening to port 3000')
})