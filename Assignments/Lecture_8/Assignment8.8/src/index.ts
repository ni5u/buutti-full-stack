import express, { Request, Response} from 'express'
import {logger, unknownEndpoint, validatePost} from './middleware'

const server = express() // server is of type "Express"
const students: {'id': number, 'name': string,'email': string }[] = [] // Tässä voisi käyttää interfacea, jolla tyypittää array

server.use(express.json())
server.use(logger)
server.use('/student',validatePost)

server.get('/students', (_req: Request, res: Response) => {
    let output = ''
    for(const i in students) {
        output += students[i].id + '\n' 
    }
    res.send(output)   
})

server.get('/student/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find((item) => item.id === id)
    if(!student) {
        res.status(404).send('id not found')
    }
    
    res.status(200).send(student)   
})

server.post('/student', (req: Request, res: Response) => {
    const id = Number(req.body.id)
    const name = req.body.name
    const email = req.body.email
    if(students.find((student) => student.id === id)) {
        res.status(400).send('Duplicate id!')
    } else {
        students.push({'id': id, 'name': name, 'email': email})
        res.status(201).send()
    }        
})

server.use(unknownEndpoint)

server.listen(3000, () => {
    console.log('Listening to port 3000')
})