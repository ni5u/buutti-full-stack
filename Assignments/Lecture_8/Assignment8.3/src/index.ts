import express, { Request, Response } from 'express'

const server = express() // server is of type "Express"
let counter = 0

server.listen(3000, () => {
    console.log('Listening to port 3000')
})


server.get('/counter', (_req: Request, res: Response) => {
    
    if('number' in _req.query) {
        counter = Number(_req.query.number)
    } else {
        counter++
    }
    res.send({counter})

})
