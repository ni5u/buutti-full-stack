import { useState, useEffect } from 'react'
import './App.css'

function App() {
  const [count, setCount] = useState(0)
  const [minutes, setMinutes]  = useState(0)
  const [timer, setTimer] = useState(true)

  useEffect(() => {
    console.log(timer)
    if(!timer) return
    const timeout = setTimeout(() => setCount(count+1),100) // onko nää "debug-sekunteja"?

    if(count === 60) {
      setMinutes(minutes + 1)
      setCount(0)
    }

    return () =>  clearTimeout(timeout)
}, [count])

  const handleReset = () => {
    setCount(0)
    setMinutes(0)
  }

  const handleTimer = () => {

    console.log(timer)
    setTimer(!timer)
  }


  return (
    <>
      <div>
      </div>
      <h1>Timer</h1>
      <div className="timer"> { minutes }:{ count } </div>
      <div>
        <button onClick={handleReset}>Reset</button>
        {/* <button onClick={handleReset}>Add minute</button>
        <button onClick={handleReset}>Add second</button> */}
        <button onClick={handleTimer}>Start/Stop</button>
      </div>
    </>
  )
}

export default App
