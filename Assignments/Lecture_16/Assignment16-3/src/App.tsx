import { useState, useEffect } from 'react'
import axios from 'axios'
import './App.css'

const url = 'https://api.api-ninjas.com/v1/dadjokes'

const config = {
  headers: {
    'X-Api-Key': 'qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM'
  }  
}

function App() {

  const[joke, setJoke] = useState('')

  useEffect(() => {
    const getJokes = async () => {
      const response = await axios.get(url, config)
      setJoke(response.data[0].joke)
    }  
    getJokes()

},[])

  return (
    <>
      <h1>Dad Jokes</h1>
      <div className="dadjokes">
        {joke}
      </div>

    </>
  )
}

export default App
