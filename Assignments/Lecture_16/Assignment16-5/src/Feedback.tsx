import { useState } from 'react'

function Feedback() {

    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [feedback, setFeedback] = useState("")
    const [checkfeedback, setCheckfeedback] = useState("feedback")

    const onOptionChance = e => { // implicit any
        setCheckfeedback(e.target.value)
    }

    const check = () => {
        
        if((!name || !email || !feedback || !checkfeedback)) {
        return true
        }
        return false
        // jos sulla on ehtolause, ja palautetaan true, jos se on true ja false jos se on false, niin voi vaan palauttaa ehtolauseen tuloksen
        // return !name || !email || !feedback || !checkfeedback
    }

    const reset = () => {
        setName('')
        setEmail('')
        setFeedback('')
        setCheckfeedback('')
    }

    const onSubmit = e => {
        e.preventDefault() // tässä se tarvitaan, koska kyseessä on submit-nappi :)

        console.log('Feedback type: ' + checkfeedback)
        console.log('Feedback: ' + feedback)
        console.log('Name:' + name)
        console.log('Email:' + email)
    };


    return (
        <>
            <div>
                <form onSubmit={onSubmit}>
                    <input
                        type='radio'
                        value='feedback'
                        name='feedback'
                        onChange={onOptionChance}
                        checked={checkfeedback === 'feedback'}
                        required />
                    Feedback
                    <br />
                    <input
                        type='radio'
                        value='suggestion'
                        name='feedback'
                        onChange={onOptionChance}
                        checked={checkfeedback === 'suggestion'}
                        required />
                    Suggestion
                    <br />
                    <input
                        type='radio'
                        value='question'
                        name='feedback'
                        onChange={onOptionChance}
                        checked={checkfeedback === 'question'}
                        required />
                    Question
                    <br />
                    Feedback:
                    <textarea
                        value={feedback}
                        rows={5}
                        cols={100}
                        onChange={e => { setFeedback(e.target.value); }}
                        required>
                    </textarea>
                    <br />
                    Name:
                    <input
                        type="text"
                        value={name}
                        onChange={e => { setName(e.target.value); }} />
                    <br />
                    Email:
                    <input
                        type="email"
                        value={email}
                        onChange={e => { setEmail(e.target.value); }} />
                    <br />
                    <input
                        type="submit" value="Send" disabled={check()} />
                    <button onClick={reset}>Reset</button>
                </form>

            </div>
        </>
    )
}

export default Feedback