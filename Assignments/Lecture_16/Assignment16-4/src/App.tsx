import { useState } from 'react'
import './App.css'


function App() {

    const [numbers, setNumbers] = useState<number[]>([])

    const addNumber = async () => {
        const newNumber = Math.floor(Math.random() * 75) + 1
        setNumbers([...numbers, newNumber])
    }

    return (
        <>
            <h1>Bingo</h1>
            <div>
                <ul>
                    {numbers.map((number, i) => <li key={i}> {number} </li>)}
                </ul>
                <button onClick={addNumber}>Add number</button>
            </div>
        </>
    )
}

export default App
