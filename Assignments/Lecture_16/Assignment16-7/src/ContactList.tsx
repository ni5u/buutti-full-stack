import { useState, ChangeEvent } from "react"

interface Contact {
	contactId: number
	name: string
	email: string
	phone: string
	address: string
	website: string
	notes: string
}

interface Props {
    contacts: Contact[]
    selectContact: (selectedContact: Contact) => void
}

function ContactList({contacts, selectContact }: Props) {

    const [nameFilter, setNameFilter] = useState('')

    const filterNames = (event: ChangeEvent<HTMLInputElement>) => {
        event.preventDefault()
        setNameFilter(event.target.value)
    }

    const filteredContacts = contacts.filter((contact) => contact.name.toLowerCase().search(nameFilter.toLowerCase()) >= 0)
    const contactList = nameFilter===''?contacts:filteredContacts
    return (
        <>
            <input type='text' value={nameFilter} onChange={filterNames} />
            <ul className='contactList'>
                {contactList.map((contact,i) => <li onClick={() => selectContact(contact)} key={'contacts:' + contact.contactId + i } >{contact.name}</li>)}
            </ul>
        </>
    )

}

export default ContactList