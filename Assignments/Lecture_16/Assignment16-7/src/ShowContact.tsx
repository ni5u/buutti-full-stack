interface Props {
    contact: Contact
    removeContact: (index: number) => void
    editContact: (contact: Contact) => void
}

interface Contact {
	contactId: number
	name: string
	email: string
	phone: string
	address: string
	website: string
	notes: string
}

function ShowContact({ contact, removeContact, editContact }: Props) {

    return (
        <div className='showContact'>
            <h2 className='contactName'>{contact.name}</h2>
            <ul className='contactInfo'>
                {contact.phone && <li><i>Phone: </i>{contact.phone}</li> }
                {contact.email && <li><i>Email: </i>{contact.email}</li> }
                {contact.address && <li><i>Address: </i>{contact.address}</li> }
                {contact.website && <li><i>Website: </i>{contact.website}</li> }
                {contact.notes && <li><i>Notes: </i>{contact.notes}</li> }
            </ul>
            <button onClick={()=> editContact(contact)}>Edit</button><button onClick={ () => removeContact(contact.contactId) }>Remove</button>
        </div>
    )
}

export default ShowContact
