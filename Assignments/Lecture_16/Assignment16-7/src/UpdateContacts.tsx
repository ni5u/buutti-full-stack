import { useState } from "react";

interface Contact {
	contactId: number
	name: string
	email: string
	phone: string
	address: string
	website: string
	notes: string
}

interface Props {
	editedContact: Contact
	updateContacts: (contact: Contact, newContact: boolean) => void
	cancel: () => void
	newContact: boolean
}

function UpdateContacts({ editedContact, updateContacts, cancel, newContact }: Props) {

	const [name, setName] = useState(newContact ? '' : editedContact.name)
	const [email, setEmail] = useState(newContact ? '' : editedContact.email)
	const [phone, setPhone] = useState(newContact ? '' : editedContact.phone)
	const [address, setAddress] = useState(newContact ? '' : editedContact.address)
	const [website, setWebsite] = useState(newContact ? '' : editedContact.website)
	const [notes, setNotes] = useState(newContact ? '' : editedContact.notes)

	const handleSubmit = (e: React.FormEvent) => {
		e.preventDefault()
		const contact: Contact = { contactId: newContact ? Infinity : editedContact.contactId, name, email, phone, address, website, notes }

		updateContacts(contact, newContact)
	}

	const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target

		switch (name) {
			case 'name':
				setName(value)
				break
			case 'email':
				setEmail(value)
				break
			case 'phone':
				if(!isNaN(Number(value))) setPhone(value)
				break
			case 'address':
				setAddress(value)
				break
			case 'website':
				setWebsite(value)
				break
			default: // onks tää täällä vaan jotta lintteri ei valita :D
				break
		}
	}

	const handleNotesChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {

		setNotes(e.target.value)
	}

	return (
		<>
			{newContact ? <h2>Add a Contact</h2> : <h2>Edit Contact</h2>}
			<form onSubmit={handleSubmit}>
				<label>Name:   <br /> <input type="text" name="name" value={name} onChange={handleInputChange} required /> </label> <br />
				<label>Email:  <br /> <input type="email" name="email" value={email} onChange={handleInputChange} /> </label> <br />
				<label>Phone:  <br /> <input type="tel" name="phone" value={phone} onChange={handleInputChange} /> </label> <br />
				<label>Address:<br /> <input type="text" name="address" value={address} onChange={handleInputChange} /> </label> <br />
				<label>Website:<br /> <input type="url" name="website" value={website} onChange={handleInputChange} /> </label> <br />
				<label>Notes:  <br /> <textarea name="notes" value={notes} onChange={handleNotesChange} /> </label> <br />
				<button type="submit">Save</button><button onClick={cancel}>Cancel</button>
			</form>
		</>
	)
}

export default UpdateContacts