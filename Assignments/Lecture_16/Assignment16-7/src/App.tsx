import { useState } from 'react'
import './App.css'
import ContactList from './ContactList'
import ShowContact from './ShowContact'
import UpdateContacts from './UpdateContacts'

interface Contact {
	contactId: number
	name: string
	email: string
	phone: string
	address: string
	website: string
	notes: string
}


function App() {

	const defaultContact: Contact = { contactId: 0, name: 'default', phone: '', address: '', email: '', notes: '', website: '' }

	type appStates = 'listContacts' | 'showContact' | 'addContact' | 'editContact'

	const [contacts, setContacts] = useState<Contact[]>([])
	const [appState, setAppState] = useState<appStates>('listContacts')
	const [selectedContact, setSelectedContact] = useState<Contact>(defaultContact)
	const [nextId, setNextId] = useState(contacts.length + 1)

	const selectContact = (contact: Contact) => {
		setSelectedContact(contact)
		setAppState('showContact')
	}

	const removeContact = (index: number) => {
		const newContacts = contacts.filter((c) => c.contactId !== index)

		setContacts(newContacts)
		setAppState('listContacts')
	}

	const updateContacts = (editedContact: Contact, newContact: boolean) => {
		if (newContact) {
			setContacts([...contacts, { ...editedContact, contactId: nextId }])
			setNextId(nextId + 1)
		} else {
			const newContacts = contacts.map((oldContact) => oldContact.contactId===editedContact.contactId?editedContact:oldContact)
			setContacts(newContacts)
		}
		setAppState('listContacts')

	}

	const editContact = (contact: Contact) => {
		setSelectedContact(contact)
		setAppState('editContact')
	}


	return (
		<div className="container">
			<div className="left-column">
				<div className="contacts">
					<ContactList contacts={contacts} selectContact={selectContact} />
				</div>
				<button onClick={() => setAppState('addContact')}>Add Contact</button>
			</div>
			<div className="right-column">
				{appState === 'listContacts' && <h2>Contact Manager</h2>}
				{appState === 'showContact' && <ShowContact contact={selectedContact} removeContact={removeContact} editContact={editContact} />}
				{appState === 'addContact' && <UpdateContacts editedContact={selectedContact}  updateContacts={updateContacts} cancel={() => setAppState('listContacts')} newContact={true} />}
				{appState === 'editContact' && <UpdateContacts editedContact={selectedContact} updateContacts={updateContacts} cancel={() => setAppState('listContacts')} newContact={false} />}
			</div>
		</div>
	)
}

export default App


// Solidia koodia. Helppo lukee, toimii moitteetta. Hyvin käytetty TS tyyppejä ja organisoitu koodia.