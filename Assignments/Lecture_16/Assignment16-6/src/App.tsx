import { useState, ChangeEvent, useEffect } from 'react'
import axios from 'axios'
import './App.css'

function App() {

	const [catSays, setCatSays] = useState('')
	const [catImage, setCatImage] = useState('')
	const [textInput, setTextInput] = useState('')
	const [refresh, setRefresh] = useState(false)

	const catURL = 'https://cataas.com/cat'
	const catSaysURL = 'https://cataas.com/cat/says/'
	const asJSON = '?json=true'

	const handleInput = (event: ChangeEvent<HTMLInputElement>) => {

		event.preventDefault()
		setTextInput(event.target.value)
	}

	const handleClick = () => {
		setCatSays(textInput)
		setTextInput('')
		setRefresh(!refresh)
	}

	useEffect(() => {
		const getCat = async () => {
			let fetchURL
			if(catSays === '') fetchURL = catURL
			else fetchURL = catSaysURL + catSays
			fetchURL += asJSON
			const response = await axios.get(fetchURL)
			const imgSRC = 'https://cataas.com/' + response.data.url
			setCatImage(imgSRC)
		}
		getCat()

	}, [catSays, refresh])


	return (
		<>
			<h1>Cat</h1>
			<div className="cats">
				<img src={catImage} />
				<input type='text' value={textInput} onChange={handleInput} />
                {/* Ei käytetä paragrapheja muotoiluun, sitä varten on CSS */}
                
				<button name='reload' onClick={handleClick}>Reload</button>
			</div>
		</>
	)
}

export default App
