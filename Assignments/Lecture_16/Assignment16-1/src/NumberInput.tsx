import { useState, ChangeEvent } from 'react'

interface Props {
    addNumber: (phoneNumber: string) => void
}
 
function NumberInput({ addNumber }:Props) {

  const [phonenumber, setPhoneNumber] = useState('')

  const inputHandler =  (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault() // tää on tarpeeton
   
    const newNum = event.target.value
    
    if(phonenumber.length === 10) {
        addNumber(phonenumber)
        setPhoneNumber('')
    } else {
        setPhoneNumber(newNum)
    }
  }

  return (
    <>
        <div className="inputHolder">
            <input name='newPhoneNumberText' onChange={inputHandler} value={phonenumber} type='number'></input>
        </div>
    </>
  )
}

export default NumberInput