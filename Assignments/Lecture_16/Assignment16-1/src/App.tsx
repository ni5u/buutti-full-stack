import { useState } from 'react'
import './App.css'
import NumberInput from './NumberInput'
 
function App() {

  const [numbers, setNumbers] = useState<string[]>([])

  const addNumber = (phoneNumber: string) => {

    setNumbers([...numbers, phoneNumber])
  }

  return (
    <>
      <h1>Phonenumbers</h1>
      <div className="card">
        <NumberInput addNumber={ addNumber } />
        <ul className="numbers">
          {numbers.map((phoneNum, i) => <div key={i}> { phoneNum } </div>)}
        </ul>
       </div>
    </>
  )
}

export default App
