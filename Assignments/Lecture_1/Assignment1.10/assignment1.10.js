const lastName = "Möttönen"
const age = 40
const isDoctor = true
const sender = "Yks Jorma vaa"
let ageOrdinal = ["th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"][(age + 1) % 10] // Kikkailuosio
// on kyll aikamoinen kikkailu
// ei me haluta kirjottaa th noin monta kertaa, siinä tulee hiki ja virheitä
// let sijaan const, kun arvo ei muutu
/*
switch((age+1)%10) {
    case 1: 
        ageOrdinal = "st"
        break
    case 2:
        ageOrdinal = "nd"
        break
    case 3:
        ageOrdinal = "rd"
        break
    default:
        ageOrdinal = "th"
}
*/

const nextAge = (age+1).toString() + ageOrdinal
console.log(`Dear ${isDoctor?"Dr.":"Mx."} ${lastName}`)
console.log()
console.log(`Congratulations on your ${nextAge} birthday! Many happy returns!`)
console.log()
console.log("Sincerely,")
console.log(sender)