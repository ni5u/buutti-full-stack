// a 
const playerCount = "4"
if (playerCount === 4 ){
    console.log("Hearts can be played")
}
else {
    console.log("Hearts cannot be played")
}

// b

const isStressed = true
const hasIcecream = true

if(!isStressed || hasIcecream){
    console.log("Mark is happy")
} else {
    console.log("Mark is unhappy")
}

// c

const isShining = true
const isRaining = false
const temperature = 20

if(isShining && !isRaining && temperature >= 20) {
    console.log("Beach day")
} else {
    console.log("Not beach day")
}

// d 

const seesSuzy = false
const seesDan = true
const weekday = "tuesday"

if(weekday === "tuesday" && seesSuzy != seesDan) {
    // ^ kuten luennolla totesin, jälkimmäinen ehto on hieman epäintuitiivinen (mutta niin on toisaalta tehtäväkin)
    console.log("Happy")
} else {
    console.log("Sad")
}