const str1 = "First string"
const str2 = "second string"
const str3 = str1 + str2
const avgLength = (str1 + str2).length / 2
// ^ tässä toki kannattaa käyttää str3 eikä laskea summaa uudestaan

console.log(str3)

console.log(str1, str1.length)
console.log(str2, str2.length)
console.log(str3, str3.length)

console.log(avgLength)

console.log(str1.toUpperCase())
console.log(str2[0], str2[str2.length-1])