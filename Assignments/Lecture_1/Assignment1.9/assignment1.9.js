const days = 365
const hours = 24
const minutes = 60
const seconds = 60
const secondsInAYear = days * hours * minutes * seconds

console.log(`Days: ${days}, hours: ${hours}, minutes: ${minutes}, seconds: ${seconds}, seconds in a year: ${secondsInAYear}`)