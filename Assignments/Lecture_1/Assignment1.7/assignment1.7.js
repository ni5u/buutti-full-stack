const price = 100
const discount = 9

console.log(`Original price ${price} €. Discount percentage ${discount} %. Discounted price ${price *
 (100 - discount) / 100} €.`)