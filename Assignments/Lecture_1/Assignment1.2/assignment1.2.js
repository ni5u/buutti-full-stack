const this_is_string = "this is a string"
let this_is_number = 10
const this_is_boolean = true

console.log(this_is_string, typeof this_is_string)
console.log()

console.log(this_is_number, typeof this_is_number)
console.log()

console.log(this_is_boolean, typeof this_is_boolean)
console.log()

// this_is_number is uses let so it can be changed
this_is_number = 100
// this won't work
//this_is_boolean = false
console.log(this_is_string, typeof this_is_string)
console.log()

console.log(this_is_number, typeof this_is_number)
console.log()

console.log(this_is_boolean, typeof this_is_boolean)
console.log()
