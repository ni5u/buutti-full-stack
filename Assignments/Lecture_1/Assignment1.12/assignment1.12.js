const people = process.argv[2]
const groupSize = process.argv[3]
let groups
// nää voi olla samalla rivillä
groups = Math.ceil(people / groupSize)
console.log("Number of groups: " + groups)

// extra
const partialGroupSize = people % groupSize
const sameSizeGroups = people / groupSize
let fullGroups = (people - partialGroupSize) / groupSize;


while(fullGroups < sameSizeGroups) {
  fullGroups++
}
// tän voi ratkaista myös käyttämättä materiaalia, jota ei vielä ole käyty läpi

console.log(`Number of groups: ${fullGroups}`)